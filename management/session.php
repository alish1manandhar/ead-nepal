<?php 
	session_start();

	$aemail = "";
	$aid = "";
	$aimage = "";

	if (!isset($_SESSION["backendStatus"])) {
		header('location: ../login/');
	}elseif ($_SESSION["backendStatus"] == 0) {
		header('location: ../login/');
	}else{
		if ($_SESSION["isAdmin"] == 0) {
			header('location: ../login/');
		}else{
			$aemail = $_SESSION["aemail"];
			$aid = $_SESSION["aid"];
			$aimage = $_SESSION["aimage"];
		}
	}
  
?>