<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php echo '<img src="'.$aimage.'" class="img-circle" style="height:45px;width:45px;" alt="User Image">'; ?>
        </div>
        <div class="pull-left info">
          <p><?php echo $aemail;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
         
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="./?page=dashboard"><i class="fa fa-circle-o"></i> Home</a></li>
            <li><a href="./?page=Balance Details"><i class="fa fa-circle-o"></i> Balance Details</a></li>
            <li><a href="./?page=Transfer Balance"><i class="fa fa-circle-o"></i> Transfer Balance</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Front End</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="./?page=Add Interest"><i class="fa fa-circle-o"></i>Add Interests</a></li>
          </ul>
        </li>
        

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Users</span>
            
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow"><?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM client WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          echo $row["COUNT"];
                      }
                  } else {
                      
                  }
                ?></small>
              <!-- <small class="label pull-right bg-green">16</small> -->
              <small class="label pull-right bg-red"><?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM redeem WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          echo $row["COUNT"];
                      }
                  } else {
                      
                  }
                ?></small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="./?page=New Client"><i class="fa fa-circle-o"></i>New Clients<span class="pull-right-container">
              <small class="label pull-right bg-yellow"><?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM client WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          echo $row["COUNT"];
                      }
                  } else {
                      
                  }
                ?></small>
              
            </span>
</a></li>
            <li><a href="./?page=client"><i class="fa fa-circle-o"></i>All Client</a></li>
            
            <li><a href="./?page=user"><i class="fa fa-circle-o"></i>Viewers</a></li>
            <li><a href="./?page=Redeem Requests"><i class="fa fa-circle-o"></i>Redeem Requests<small class="label pull-right bg-red"><?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM redeem WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          echo $row["COUNT"];
                      }
                  } else {
                      
                  }
                ?></small></a></li>
                <li><a href="./?page=Redeem History"><i class="fa fa-circle-o"></i>Redeem History</a></li>
                <li><a href="./?page=Blocked Clients"><i class="fa fa-circle-o"></i>Black Listed Clients</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-photo"></i> <span>All Ads</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="./?page=Ads"><i class="fa fa-circle-o"></i>Life Time Ads</a></li>
            <li><a href="./?page=Ads Today"><i class="fa fa-circle-o"></i>Ads Added Today</a></li>
          </ul>

        </li>

        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>