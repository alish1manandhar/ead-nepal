<?php
  session_start();
  $_SESSION["aemail"] = "";
  $_SESSION["backendStatus"] = 0;
  $_SESSION["aid"] = 0;
  $_SESSION["isAdmin"] = 0;
  $_SESSION["aimage"] = "../../dist/img/avatar".rand(1,5).".png";
  $errr = "";

  if(isset($_POST['submit'])){
    $username = $_POST['user'];
    $password = md5($_POST['pass']);
    
    include_once('../connection.php');
    $sql = sprintf("SELECT * FROM admin WHERE email = '%s' AND password ='%s'",
            mysqli_real_escape_string($conn,$username),
            mysqli_real_escape_string($conn,$password)
      );

    $result=$conn->query($sql);
    $stat = 0;

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()){
                    $id = $row['id'];
                    $_SESSION["aid"] = $id;
                    $_SESSION["aemail"] = $row['email'];
                    $_SESSION["isAdmin"] = 1;
                    $stat +=1;
      }
    }

    $count = $result->num_rows;

    if($count==1){
      $_SESSION["backendStatus"] = 1;
        if($stat == 1){
          header("location: ../united/");
        }else{
          $_SESSION["aemail"] = "";
          $_SESSION["backendStatus"] = 0;
          $_SESSION["aid"] = 0;
          $_SESSION["isAdmin"] = 0;
          $errr = '<div class="alert alert-danger"><strong>Not verified account</strong></div>';
        }
    }else{
      $errr = '<div class="alert alert-danger"><strong>Please enter valid info</strong></div>';
    }
  }
?>

<!DOCTYPE html>
<html>
 
<?php include '../headLinks.php';  ?>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>Panel</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="user" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback" ></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="pass" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <?php echo $errr?>
      <div class="row">
        <div class="col-xs-8">
          </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>   
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>