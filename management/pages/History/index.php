<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>

  <style type="text/css">
  
    h2 {
      font-size: 5em;
      margin: 0;
    }

    .download-btn {
      background-color: #1abc9c;
      border: none;
      border-bottom: 3px solid #16a085;
      font-size: 2em;
      color: #fff;
      display: none;
      padding: 10px 30px;
      cursor: pointer;
      transition: background-color 320ms ease-in-out;
    }

    .download-btn:hover, .download-btn:focus {
      background-color: #16a085;
    }
  </style>
<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">History of added Ads</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="image_tab" data-toggle="tab" href="#Image" role="tab" aria-controls="Image" aria-selected="true">Image</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="Audio-tab" data-toggle="tab" href="#Audio" role="tab" aria-controls="Audio" aria-selected="false">Audio</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="Video-tab" data-toggle="tab" href="#Video" role="tab" aria-controls="Video" aria-selected="false">Video</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active" id="Image" role="tabpanel" aria-labelledby="image_tab">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Added Date</th>
                      <th>Price</th>
                      <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?php 
                        $sql="SELECT * FROM add_ads where fk_client_id = $aid AND status = 1";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $status = "";
                                $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='width:100%;'/>";
                                  
                                echo "
                                  <tr>
                                    <td>".$row['id']."</td>
                                    <td>".$row['a_title']."</td>
                                    <td>".$row['added_date']."</td>
                                    <td>".$row['reach_out_price']."</td>
                                    <td><a href='#' data-toggle='modal' data-target='#detailModel".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Details</a></td>
                                  </tr>
                                  <!-- Modal -->
                                  <div class='modal fade' id='detailModel".$row['id']."' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true' data-keyboard='false' data-backdrop='static'>
                                    <div class='modal-dialog modal-dialog-centered' role='document'>
                                      <div class='modal-content'>
                                        <div class='modal-header'>
                                          <h5 class='modal-title' id='exampleModalLongTitle'><h3>".$row['a_title']."</h3><i>".$row['added_date']."</i></h5>
                                          
                                          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>
                                        <div class='modal-body'>
                                          <div class='row'>
                                            <div class='col-md-6'>
                                              Targeted Gender : ".$row['target_gender']." <br>  
                                              Targeted age group : ".$row['age_group']." <br>  
                                              Targeted locations : ".$row['location']."<br>
                                              Targeted Interests : ".$row['interest']."<br>
                                              Price : ".$row['reach_out_price']."<br>
                                            </div>
                                            <div class='col-md-6'>";?>

                                               
                                            <?php
                                              echo "
                                              $img
                                            </div>
                                          </div>
                                            
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                                          <button type='button' class='btn btn-primary'>Save changes</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                ";
                            }
                        } else {
                            echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                        }
                      ?>  
                      </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="Audio" role="tabpanel" aria-labelledby="Audio-tab">
                  <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Price</th>
                  <th>Details</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM add_audio_ads where fk_client_id = $aid AND status = 1";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $status = "";
                          // $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='height:50px;'/>";
                            
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td>".$row['reach_out_price']."</td>
                              <td><a href='#' data-toggle='modal' data-target='#detailAudio".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Details</a></td>
                            </tr>

                            <!-- Modal -->
                                  <div class='modal fade' id='detailAudio".$row['id']."' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true' data-keyboard='false' data-backdrop='static'>
                                    <div class='modal-dialog modal-dialog-centered' role='document'>
                                      <div class='modal-content'>
                                        <div class='modal-header'>
                                          <h5 class='modal-title' id='exampleModalLongTitle'><h3>".$row['a_title']."</h3><i>".$row['added_date']."</i></h5>
                                          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>
                                        <div class='modal-body'>
                                          <div class='row'>
                                            <div class='col-md-6'>
                                              Targeted Gender : ".$row['target_gender']." <br>  
                                              Targeted age group : ".$row['age_group']." <br>  
                                              Targeted locations : ".$row['location']."<br>
                                              Targeted Interests : ".$row['interest']."<br>
                                              Price : ".$row['reach_out_price']."<br>
                                            </div>
                                            <div class='col-md-6'>
                                              <audio controls src='../pages/target audio/uploads/".$row['audio']."' type='audio/mpeg' ></audio>
                                            </div>
                                          </div>
                                            
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                                          <button type='button' class='btn btn-primary'>Save changes</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
                <div class="tab-pane fade" id="Video" role="tabpanel" aria-labelledby="Video-tab">
                  <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Price</th>
                  <th>Details</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM add_video_ads where fk_client_id = $aid AND status = 1";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $status = "";
                          // $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='height:50px;'/>";
                            
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td>".$row['reach_out_price']."</td>
                              <td><a href='#' data-toggle='modal' data-target='#detailVideo".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Details</a></td>
                            </tr>
                            <!-- Modal -->
                                  <div class='modal fade' id='detailVideo".$row['id']."' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true' data-keyboard='false' data-backdrop='static'>
                                    <div class='modal-dialog modal-dialog-centered' role='document'>
                                      <div class='modal-content'>
                                        <div class='modal-header'>
                                          <h5 class='modal-title' id='exampleModalLongTitle'><h3>".$row['a_title']."</h3><i>".$row['added_date']."</i></h5>
                                          <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>
                                        <div class='modal-body'>
                                          <div class='row'>
                                            <div class='col-md-6'>
                                              Targeted Gender : ".$row['target_gender']." <br>  
                                              Targeted age group : ".$row['age_group']." <br>  
                                              Targeted locations : ".$row['location']."<br>
                                              Targeted Interests : ".$row['interest']."<br>
                                              Price : ".$row['reach_out_price']."<br>
                                            </div>
                                            <div class='col-md-6'>
                                              <video width='100%' controls>
                                                <source src='../pages/target video/uploads/".$row['video']."' type='video/mp4'>
                                              </video>
                                            </div>
                                          </div>
                                            
                                        </div>
                                        <div class='modal-footer'>
                                          <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                                          <button type='button' class='btn btn-primary'>Save changes</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
              </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <script type="text/javascript">
    $(function () {
  'use strict';
  var seconds = $('.seconds'),
      counDown = setInterval(counter, 1000);
  // Stop the interval
  function stop() {
    clearInterval(counDown);
  }
  
  function counter () {
    // Decrement seconds
    seconds.text(
      '0' + (seconds.text() - 1)
    );
    // Stop the interval when seconds less then or equal 0
    if (seconds.text() <= 0) {      
      stop();
      // Shown download button
      seconds.fadeOut(1000, function () {
        $('.download-btn').fadeIn('1000');
      });
    }
  }
});
  </script>