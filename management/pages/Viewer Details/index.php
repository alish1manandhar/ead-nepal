<?php
  $getid = 0;
  if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $getid = $_GET['id'];
    $name = "";
    $email = "";
    $phone = "";
    $location = "";
    $college = "";
    $level = "";
    $field_of_study = "";
    $company = "";
    $post = "";
    $interest ="";
    $age = "";
    $sex = "";
    $balance = "";

    $sql="SELECT * FROM viewers where id = $getid ";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              $state = "Deactivated";
                              if ($row['status'] == 1) {
                                $state = "Activated";
                              }
                                $name =  $row['f_name']. " " .  $row['l_name'];
                                $email =  $row['email'];
                                $phone =  $row['phone'];
                                $location =  $row['location'];
                                $college =  $row['college'];
                                $level =  $row['level'];
                                $field_of_study =  $row['field_of_study'];
                                $company = $row['company'];
                                $post = $row['post'];
                                $age = $row['age'];
                                $sex = $row['sex'];
                                $balance = $row['balance'];
                                $interest = $row['interest'];
                                // if ($row['seen'] == 0) {
                                //   $seensql = "UPDATE client set seen = 1 where id = $getid";
                                //   mysqli_query($conn,$seensql);
                                //   echo "<script>
                                //         location.reload();
                                //       </script>";
                                // }
                            }
                        } else {
                            
                        }
  }

 ?>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
  <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Redeem History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['update'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['update']; ?>
                    </div>
                  <?php
                  unset($_SESSION['update']);
                }elseif(isset($_SESSION['updateErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['updateErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['updateErr']);

                }
              ?>
              <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Redeem Date</th>
                  <th>Redeem Amount</th>
                  <th>Redeem Type</th>
                  <th>Paid Date</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="select * from redeem where userid = $getid and paid = 1";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          
                          echo "
                            <tr>
                              <td>".$row['redeemdate']."</td>
                                    <td>".$row['amount']."</td>
                                    <td>".$row['redeemtype']."</td>
                                    <td>".$row['paiddate']."</td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td>No Data</td><td>X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Redeem Requests</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Redeem Date</th>
                  <th>Redeem Amount</th>
                  <th>Redeem Type</th>
                  <th>Transfer Balance</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="select * from redeem where userid = $getid and paid = 0";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                        $uid = $row['userid'];
                        if ($row['seen'] == 0) {
                          $seensql="update redeem set seen = 1 where userid = $getid";
                          $result=mysqli_query($conn,$seensql);
                          if ($result) {
                              echo "<script>
                                    location.reload();
                                    </script>";
                          }
                        }
                        
                          echo "
                            <tr>
                              <td>".$row['redeemdate']."</td>
                                    <td>".$row['amount']."</td>
                                    <td>".$row['redeemtype']."</td>
                                    <td><a href='../pages/Redeem Requests/redeem.php?id=$uid' class='btn btn-danger' role='button'>Transfer</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td>No Data</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
  <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">History of Ads Watched</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['stat'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['stat']; ?>
                    </div>
                  <?php
                  unset($_SESSION['stat']);
                }elseif(isset($_SESSION['statErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['statErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['statErr']);

                }
              ?>
              <div class="col-md-6">
                
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav active">
                  <a class="nav-link active" id="image_tab" data-toggle="tab" href="#Image" role="tab" aria-controls="Image" aria-selected="true">Image</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Audio-tab" data-toggle="tab" href="#Audio" role="tab" aria-controls="Audio" aria-selected="false">Audio</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Video-tab" data-toggle="tab" href="#Video" role="tab" aria-controls="Video" aria-selected="false">Video</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  in active " id="Image" role="tabpanel" aria-labelledby="image_tab">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Title</th>
                      <th>Watch Date</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?php 
                        $sql="select  iv.watch_date as watch_date , a.a_title from imageviews iv, add_ads a where iv.fk_ads = a.id and iv.fk_viewer = $getid ";
                        $result=mysqli_query($conn,$sql);
                        $imagewatchs =0;
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              
                                echo "
                                  <tr>
                                    <td>".$row['a_title']."</td>
                                    <td>".$row['watch_date']."</td>
                                  </tr>
                                ";
                            }
                        } else {
                            echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                        }
                      ?>  
                      </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="Audio" role="tabpanel" aria-labelledby="Audio-tab">
                  <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Watched Date</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="select iv.watch_date as watch_date , a.a_title from audioviews iv, add_audio_ads a where iv.fk_ads = a.id and iv.fk_viewer = $getid";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                        
                          echo "
                            <tr>
                              <td>".$row['a_title']."</td>
                                    <td>".$row['watch_date']."</td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
                <div class="tab-pane fade" id="Video" role="tabpanel" aria-labelledby="Video-tab">
                  <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Watched Date</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="select iv.watch_date as watch_date , a.a_title from videoviews iv, add_video_ads a where iv.fk_ads = a.id and iv.fk_viewer = $getid";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                        
                          echo "
                            <tr>
                              <td>".$row['a_title']."</td>
                                    <td>".$row['watch_date']."</td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
              </div>  
              </div>
              <div class="col-md-6">
                <p>Name: <?php echo $name; ?></p>
                <p>Email: <?php echo $email; ?></p>
                <p>Phone: <?php echo $phone; ?></p>
                <p>Age: <?php echo $age; ?></p>
                <p>Sex: <?php echo $sex; ?></p>
                <p>Location: <?php echo $location; ?></p>
                <p>College : <?php echo $college; ?></p>
                <p>Level : <?php echo $level; ?></p>
                <p>Field of Study : <?php echo $field_of_study; ?></p>
                <p>Company: <?php echo $company; ?></p>
                <p>Post: <?php echo $post; ?></p>
                <p>Interests: <?php echo $interest; ?></p>
                <p>Balance: Nrs. <?php echo $balance; ?></p>
                
                <form id="form" action="../pages/Client Details/status.php" method="get">
                <!-- Message: <input type="text" name="messages" style="width:100%;" ><br> -->
                <div class="form-group">
                  <label for="comment">Message:</label>
                  <textarea name="msg" class="form-control" rows="5" id="comment" required></textarea>
                </div>
                
                <input type="submit" type="button" class="btn btn-danger" value="<?php echo $state; ?>"/>
                </form>
                <hr>

              </div>
              <?php echo "
              <script type='text/javascript'>
                $('#form').submit(function(eventObj) {
                    $(this).append('";?><input type="hidden" name="id" value="<?php echo $getid; ?>" /> <?php echo "');
                    return true;
                });
              </script>";?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
    </section>
