<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">History of added Ads</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['update'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['update']; ?>
                    </div>
                  <?php
                  unset($_SESSION['update']);
                }elseif(isset($_SESSION['updateErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['updateErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['updateErr']);

                }
              ?>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav active">
                  <a class="nav-link active" id="image_tab" data-toggle="tab" href="#Image" role="tab" aria-controls="Image" aria-selected="true">Image</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Audio-tab" data-toggle="tab" href="#Audio" role="tab" aria-controls="Audio" aria-selected="false">Audio</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Video-tab" data-toggle="tab" href="#Video" role="tab" aria-controls="Video" aria-selected="false">Video</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  in active " id="Image" role="tabpanel" aria-labelledby="image_tab">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Added Date</th>
                      <th>Insights</th>
                      <th>Activate/Deactivate</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?php 
                        $sql="SELECT * FROM add_ads ";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              $state = "Deactivated";
                              if ($row['status'] == 1) {
                                $state = "Activated";
                              }
                                $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='width:150px;'/>";
                                echo "
                                  <tr>
                                    <td>".$row['id']."</td>
                                    <td>".$row['a_title']."</td>
                                    <td>".$row['added_date']."</td>
                                    <td><a href='./?page=Detailed Image Insights Admin&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                                     <td><a href='../pages/User Details/status.php?id=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>".$state."</a></td>
                                  </tr>
                                ";
                            }
                        } else {
                            echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                        }
                      ?>  
                      </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="Audio" role="tabpanel" aria-labelledby="Audio-tab">
                  <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Insights</th>
                  <th>Deactive,activate</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="SELECT * FROM add_audio_ads";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                        $state = "Deactivated";
                        if ($row['status'] == 1) {
                          $state = "Activated";
                        }
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td><a href='./?page=Detailed Audio Insights Admin&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                              <td><a href='../pages/User Details/audiostatus.php?id=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>".$state."</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
                <div class="tab-pane fade" id="Video" role="tabpanel" aria-labelledby="Video-tab">
                  <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Insights</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM add_video_ads ";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                        $state = "Deactivated";
                        if ($row['status'] == 1) {
                          $state = "Activated";
                        }
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td><a href='./?page=Detailed Video Insights Admin&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                              <td><a href='../pages/User Details/videostatus.php?id=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>".$state."</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
              </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
