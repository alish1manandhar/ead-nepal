<?php
  $getid = 0;
  $img = "";
  if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $getid = $_GET['id'];
    $name = "";
    $email = "";
    $phone = "";
    $Company = "";
    $CompanyType = "";
    $number = "";
    $registeredDate = "";

    $sql="SELECT * FROM client where id = $getid ";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                              $state = "Deactivated";
                              if ($row['status'] == 1) {
                                $state = "Activated";
                              }
                                $img = $row['doc_image'];
                                $name =  $row['f_name']. " " .  $row['l_name'];
                                $email =  $row['email'];
                                $phone =  $row['phone'];
                                $Company =  $row['company_name'];
                                $CompanyType =  $row['company_type'];
                                $number =  $row['company_pan'];
                                $registeredDate =  $row['reg_date'];
                                $est_date = $row['est_date'];
                                $balance = $row['balance'];
                                if ($row['seen'] == 0) {
                                  $seensql = "UPDATE client set seen = 1 where id = $getid";
                                  mysqli_query($conn,$seensql);
                                  echo "<script>
                                        location.reload();
                                      </script>";
                                }
                            }
                        } else {
                            
                        }
  }
?>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">User Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['stat'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['stat']; ?>
                    </div>
                  <?php
                  unset($_SESSION['stat']);
                }elseif(isset($_SESSION['statErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['statErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['statErr']);

                }
              ?>
              <div class="col-md-6">
                <a href='../../home/client/uploads/<?php echo $img; ?>' target='_blank'><img src="../../home/client/uploads/<?php echo $img; ?>" style="height:300px;width:100%;object-fit: cover;"> </a>
              </div>
              <div class="col-md-6">
                <p>Name: <?php echo $name; ?></p>
                <p>Email: <?php echo $email; ?></p>
                <p>Phone: <?php echo $phone; ?></p>
                <p>Company Name: <?php echo $Company; ?></p>
                <p>Company type: <?php echo $CompanyType; ?></p>
                <p>Company Pan Number: <?php echo $number; ?></p>
                <p>Registered date: <?php echo $registeredDate; ?></p>
                <p>Established date: <?php echo $est_date; ?></p>
                <p>Balance: Nrs. <?php echo $balance; ?></p>
                <form id="formp" action="../pages/Client Details/change_password.php" method="get">
                <!-- Message: <input type="text" name="messages" style="width:100%;" ><br> -->
                <div class="form-group">
                  <label for="comment">Message:</label>
                  <textarea name="pwd" class="form-control" rows="5" id="comment" required></textarea>
                </div>
                
                <input type="submit" type="button" class="btn btn-danger" value="Change Password"/>
                </form>
                <form id="form" action="../pages/Client Details/status.php" method="get">
                <!-- Message: <input type="text" name="messages" style="width:100%;" ><br> -->
                <div class="form-group">
                  <label for="comment">Message:</label>
                  <textarea name="msg" class="form-control" rows="5" id="comment" required></textarea>
                </div>
                
                <input type="submit" type="button" class="btn btn-danger" value="<?php echo $state; ?>"/>
                </form>
                <hr>

                <form id="formb" action="../pages/Client Details/blacklist.php" method="get">
                <!-- Message: <input type="text" name="messages" style="width:100%;" ><br> -->
                <div class="form-group">
                  <label for="comment">Message:</label>
                  <textarea name="msgb" class="form-control" rows="5" id="comment" required></textarea>
                </div>
                
                <input type="submit" type="button" class="btn btn-danger" value="Black list user"/>
                </form>
              </div>
              <?php echo "
              <script type='text/javascript'>
                $('#form').submit(function(eventObj) {
                    $(this).append('";?><input type="hidden" name="id" value="<?php echo $getid; ?>" /> <?php echo "');
                    return true;
                });
              </script>";?>
              <?php echo "
              <script type='text/javascript'>
                $('#formb').submit(function(eventObj) {
                    $(this).append('";?><input type="hidden" name="idb" value="<?php echo $getid; ?>" /> <?php echo "');
                    return true;
                });
              </script>";?>
              <?php echo "
              <script type='text/javascript'>
                $('#formp').submit(function(eventObj) {
                    $(this).append('";?><input type="hidden" name="idp" value="<?php echo $getid; ?>" /> <?php echo "');
                    return true;
                });
              </script>";?>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
