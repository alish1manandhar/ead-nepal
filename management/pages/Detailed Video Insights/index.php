<?php 
    if (isset($_GET['pid'])) {
      $pid = $_GET['pid'];
      $title = "";
      $img = "";
      $initialPrice = 0;
      $reach_out_price = 0;

      $sql="SELECT * FROM add_video_ads where fk_client_id = $aid and id = $pid";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['video']."' style='width:150px;'/>";
                                $title = $row['a_title'];
                                $initialPrice = $row['initial_price'];
                                $reach_out_price = $row['reach_out_price'];
                            }
                        } else {
                            echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                        }
    }
    
?>
<style type="text/css">
  /*slider from here*/
      .slidecontainer {
          width: 100%;
      }

      .slider {
          -webkit-appearance: none;
          width: 100%;
          height: 25px;
          background: #d3d3d3;
          outline: none;
          opacity: 0.7;
          -webkit-transition: .2s;
          transition: opacity .2s;
      }

      .slider:hover {
          opacity: 1;
      }

      .slider::-webkit-slider-thumb {
          -webkit-appearance: none;
          appearance: none;
          width: 25px;
          height: 25px;
          background: #4CAF50;
          cursor: pointer;
      }

      .slider::-moz-range-thumb {
          width: 25px;
          height: 25px;
          background: #4CAF50;
          cursor: pointer;
      }

    p{text-align:center;}
    .label {
      height: 1em;
      padding: .3em;
      background: rgba(255, 255, 255, .8);
      position: absolute;
      display: none;
      color:#333;
      
    }
</style>
<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3><br>
              <td><a href='javascript:history.go(-1)' ><i class='fa fa-arrow-left'></i> Go Back </a></td>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <label>Remaining Budget <br> 1 view = Nrs. 1 </label> <br>
                      <div class="slidecontainer">
                      <input name="reach_out" step="0.1" type="range" min="0" max="<?php echo $initialPrice;?>" value="<?php echo $reach_out_price;?>" class="slider" id="myRange" disabled>
                      <p style="font-size:25px;">Left Rs. <b><span id="demo"></span></b> of Initial Price Rs.<?php echo $initialPrice;?></p>
                <hr>
                <h3 class="box-title">View Insights</h3>

                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Views Over the year</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                      <canvas id="areaChart" style="height:250px"></canvas>
                    </div>
                  </div>

                  <!-- /.box-body -->
                </div>

                <h3 class="box-title">Todays Viewers</h3>
                <hr>
                <h3 style='text-align:right;'>
                                              <?php 
                                              $imageId = $row['id'];
                                              $countViews="SELECT count(fk_viewer) as count FROM videoviews where fk_ads = $pid and watch_date = CURDATE()";
                                              $countResult=mysqli_query($conn,$countViews);
                                              if ($countResult->num_rows > 0) {
                                                while($counter = $countResult->fetch_assoc()) {
                                                  echo $counter['count'];
                                                  echo " Views</h3>";
                                                }}

                                              echo "<div class='row'>
                                                      <div class='col-md-1'>ID</div>
                                                      <div class='col-md-6'>Email</div>
                                                      <div class='col-md-4'>Date</div><br>
                                                      <hr>";
                                              
                                                      $detailSql="SELECT iv.fk_viewer,v.email,iv.watch_date FROM videoviews iv,viewers v where iv.fk_ads = $pid and iv.fk_viewer = v.id and iv.watch_date = CURDATE() LIMIT 100";
                                                      $detailResult=mysqli_query($conn,$detailSql);

                                                      if ($detailResult->num_rows > 0) {
                                                        while($data = $detailResult->fetch_assoc()) {
                                                          
                                                          ?>
                                                            <div class="col-md-1"><?php echo $data['fk_viewer']; ?></div>
                                                            <div class="col-md-6"><?php echo $data['email']; ?></div>
                                                            <div class="col-md-4"><?php echo $data['watch_date']; ?></div><br>
                                                          <?php
                                                        }
                                                      } 
                                                    ?>
            </div>
            <h3 class="box-title">Viewers From Different Places This Month</h3>
                <hr>
                <h3 style='text-align:right;'>
                                              <?php 
                                              $imageId = $row['id'];
                                              $countViews="SELECT  count(fk_viewer) as COUNT FROM videoviews where fk_ads = $pid and MONTH(watch_date) = MONTH(CURDATE()) and YEAR(watch_date) = YEAR(CURDATE()) ";
                                              $countResult1=mysqli_query($conn,$countViews);
                                              if ($countResult1->num_rows > 0) {
                                                while($counter1 = $countResult1->fetch_assoc()) {
                                                  echo $counter1['COUNT'];
                                                  echo " Views</h3>";
                                                }}

                                              echo "<div class='row'>
                                                      <div class='col-md-1'>location</div>
                                                      <div class='col-md-6'>Month</div>
                                                      <div class='col-md-4'>Total Viewers</div><br>
                                                      <hr>";
                                              
                                                      $detailSql1="SELECT V.location AS LOCATION, count(iv.fk_viewer) as COUNT, MONTHNAME(iv.watch_date) as MONTHNAME,MONTH(iv.watch_date) as MONTHNUMBER FROM videoviews iv, viewers v where iv.fk_ads = $pid and MONTH(iv.watch_date) = MONTH(CURDATE()) and YEAR(iv.watch_date) = YEAR(CURDATE()) and iv.fk_viewer = v.id group by MONTH(iv.watch_date), v.location";
                                                      $detailResult1=mysqli_query($conn,$detailSql1);

                                                      if ($detailResult1->num_rows > 0) {
                                                        while($data = $detailResult1->fetch_assoc()) {
                                                          
                                                          ?>
                                                            <div class="col-md-1"><?php echo $data['LOCATION']; ?></div>
                                                            <div class="col-md-6"><?php echo $data['MONTHNAME']; ?></div>
                                                            <div class="col-md-4"><?php echo $data['COUNT']; ?></div><br>
                                                          <?php
                                                        }
                                                      } 
                                                    ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <script type="text/javascript">
      var datas = [<?php
                    
                    $sql ="SELECT YEAR(watch_date) AS YEAR , MONTH(watch_date) AS NUMBER, MONTHNAME(watch_date) AS MONTH, COUNT(id) AS VIEWS FROM videoviews WHERE  fk_ads = $pid and YEAR(watch_date) = YEAR(CURDATE()) GROUP BY YEAR(watch_date), MONTH(watch_date) ORDER BY 1,2";
                    $res = mysqli_query($conn,$sql);
                    $row = [0,0,0,0,0,0,0,0,0,0,0,0];
                    while($r = mysqli_fetch_assoc($res)) {
                      $m = $r['NUMBER'] -1;
                      $row[$m] = $r['VIEWS'];
                    }

                    $tag_strings = array();

                    $Tags = $row;
                    foreach($Tags as $tag){
                          $tag_strings[] = $tag;
                    }
                    echo implode(",", $tag_strings);
                    ?>];
    </script>
<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- ChartJS -->
<script src="../../bower_components/chart.js/Chart.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
    <script>
      var slider = document.getElementById("myRange");
      var output = document.getElementById("demo");
      output.innerHTML = slider.value;

      slider.oninput = function() {
        output.innerHTML = this.value;
      }
    </script>
    <script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)
    
    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','Octomber','November','December'],
      datasets: [
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : datas
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)

    
  })
</script>
