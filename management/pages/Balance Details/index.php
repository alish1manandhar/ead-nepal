<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Todays Balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table" style="text-align:center">
                <thead>
                  <tr >
                    <th style="text-align:center" scope="col">Image Ads</th>
                    <th style="text-align:center" scope="col">Video Ads</th>
                    <th style="text-align:center" scope="col">Audio Ads</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php include 'imageAdstoday.php';?></td>
                    <td><?php include 'videoAdstoday.php';?></td>
                    <td><?php include 'audioAdstoday.php';?></td>
                  </tr>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table" style="text-align:center">
                <thead>
                  <tr >
                    <th style="text-align:center" scope="col">Image Ads</th>
                    <th style="text-align:center" scope="col">Video Ads</th>
                    <th style="text-align:center" scope="col">Audio Ads</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php include 'imageAdsMonthly.php';?></td>
                    <td><?php include 'videoAdsMonthly.php';?></td>
                    <td><?php include 'audioAdsMonthly.php';?></td>
                  </tr>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Life Time Balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table" style="text-align:center">
                <thead>
                  <tr >
                    <th style="text-align:center" scope="col">Image Ads</th>
                    <th style="text-align:center" scope="col">Video Ads</th>
                    <th style="text-align:center" scope="col">Audio Ads</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php include 'imageAds.php';?></td>
                    <td><?php include 'videoAds.php';?></td>
                    <td><?php include 'audioAds.php';?></td>
                  </tr>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
          <!-- /.box -->
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Email</th>
                  <th>Transfered Date</th>
                  <th>Profit</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT c.email, a.transfered_date, a.balance FROM admin_balance a, viewers c where a.fk_viewer_id = c.id and YEAR(transfered_date) = YEAR(CURDATE()) AND MONTH(transfered_date) = MONTH(CURDATE())";
                  if ($result=mysqli_query($conn,$sql)){
                      while ($row = mysqli_fetch_assoc($result)) {
                        echo "
                            <tr>
                              <td>".$row['email']."</td>
                              <td>".$row['transfered_date']."</td>
                              <td>".$row['balance']."</td>";
                    }
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
