    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Company Name</th>
                  <th>Account Details</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM client";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $userid = $row['id'];
                          $statu = $row['status'];
                          $img = "<a href='../../home/client/uploads/".$row['doc_image']."' target='_blank'><img alt='e-ads' src='../../home/client/uploads/".$row['doc_image']."' style='height:50px;'/></a>
";
                          $stat = "Activated";
                          if ($statu == 0) {
                            $stat = "Deactivated";
                          }else if($statu == 100){
                            $stat = "Blocked";
                          }else{
                            $stat = "Activated";
                          }

                          
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$img."</td>
                              <td><a href='./?page=Client Details&id=$userid'>".$row['f_name']." ".$row['l_name']."  <i class='fa fa-hand-o-up'></a></td>
                              <td>".$row['email']."</td>
                              <td>".$row['phone']."</td>
                              <td>".$row['company_name']."</td>
                              <td><a href='./?page=User Details&id=$userid'>Uploads <i class='fa fa-hand-o-up'></i></a></td>
                              <td>$stat</td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> 4</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
 