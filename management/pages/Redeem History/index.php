<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
  <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Redeem History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Viewer</th>
                  <th>Redeem Date</th>
                  <th>Redeem Amount</th>
                  <th>Redeem Type</th>
                  <th>Paid Date</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="select * from viewers v, redeem r where v.id = r.userid and r.paid = 1";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $uid = $row['userid'];
                          echo "
                            <tr>
                              <td><a href='../united/?page=Viewer%20Details&id=$uid'>".$row['f_name']." ".$row['l_name']."</a> <i class='fa fa-hand-o-up'></i></td>
                              <td>".$row['redeemdate']."</td>
                                    <td>".$row['amount']."</td>
                                    <td>".$row['redeemtype']."</td>
                                    <td>".$row['paiddate']."</td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td>No Data</td><td>X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
   
    </section>
