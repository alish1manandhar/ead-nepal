    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['update'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['update']; ?>
                    </div>
                  <?php
                  unset($_SESSION['update']);
                }elseif(isset($_SESSION['updateErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['updateErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['updateErr']);

                }
              ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Interest</th>
                  <th>Details</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM viewers";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $userid = $row['id'];
                          
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['f_name']." ".$row['l_name']."</td>
                              <td>".$row['email']."</td>
                              <td>".$row['phone']."</td>
                              <td>".$row['interest']."</td>
                              <td><a href='./?page=Viewer Details&id=$userid'>View Details</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> 4</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
 