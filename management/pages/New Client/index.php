    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List of users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if (isset($_SESSION['stat'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['stat']; ?>
                    </div>
                  <?php
                  unset($_SESSION['stat']);
                }elseif(isset($_SESSION['statErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['statErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['statErr']);

                }
              ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Company Name</th>
                  <th>Company Type</th>
                  <th>Company Pan</th>
                  <th>Account Details</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM client where status = 0";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $userid = $row['id'];
                          $statu = $row['status'];
                          $img = "<a href='../../home/client/uploads/".$row['doc_image']."' target='_blank'><img alt='e-ads' src='../../home/client/uploads/".$row['doc_image']."' style='height:50px;'/></a>
";
                          $stat = "Activated";
                          if ($statu == 0) {
                            $stat = "Deactivated";
                          }else{
                            $stat = "Activated";
                          }

                          
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$img."</td>
                              <td><a href='./?page=Client Details&id=$userid'>".$row['f_name']." ".$row['l_name']."</a></td>
                              <td>".$row['email']."</td>
                              <td>".$row['phone']."</td>
                              <td>".$row['company_name']."</td>
                              <td>".$row['company_type']."</td>
                              <td>".$row['company_pan']."</td>
                              <td><a href='./?page=User Details&id=$userid'>View Details</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> 4</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
 