    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <h2>Total In Life Time</h2>
      <div class="row">
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'userCount.php';?></h3>

              <p>Total Viewers</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="./?page=user" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'clientCount.php';?></h3>

              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="./?page=participants" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'imageAds.php';?></h3>

              <p>Image Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-photo-o"></i>
            </div>
            <a href="./?page=photoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'videoCount.php';?></h3>

              <p>Video Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-video-o"></i>
            </div>
            <a href="./?page=videoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'audioCount.php';?></h3>

              <p>Audio Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-audio-o"></i>
            </div>
            <a href="./?page=blogList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'imageviewsCount.php';?></h3>

              <p>Image Ads Views</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-photo-o"></i>
            </div>
            <a href="./?page=photoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'videoviewsCount.php';?></h3>

              <p>Video Ads Views</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-video-o"></i>
            </div>
            <a href="./?page=videoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'audioviewsCount.php';?></h3>

              <p>Audio Ads Views</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-audio-o"></i>
            </div>
            <a href="./?page=blogList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <h2>Today</h2>
      <div class="row">
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'userCounttoday.php';?></h3>

              <p>Total Viewers</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="./?page=user" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'clientCounttoday.php';?></h3>

              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="./?page=participants" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'imageAdstoday.php';?></h3>

              <p>Image Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-photo-o"></i>
            </div>
            <a href="./?page=photoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'videoCounttoday.php';?></h3>

              <p>Video Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-video-o"></i>
            </div>
            <a href="./?page=videoList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php include 'audioCounttoday.php';?></h3>

              <p>Audio Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-audio-o"></i>
            </div>
            <a href="./?page=blogList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
 