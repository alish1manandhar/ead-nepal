<?php 
  //get Post
    if(isset($_POST['submit'])){
      $name = mysqli_real_escape_string($conn,$_POST['title']);
      if($name == "" ){
        echo "<script type='text/javascript'>alert('Invalid inputs fields are empty!!');</script>";
      }else{
        $img = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];
        $newfilename= date('dmYHis').str_replace(" ", "", basename($img));
        $upload_dir = '../pages/Add interest/uploads/'; // upload directory
                                $imgExt = strtolower(pathinfo($img,PATHINFO_EXTENSION)); // get image extension
                                $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions

        if(in_array($imgExt, $valid_extensions)) {
          move_uploaded_file($tmp_dir,$upload_dir.$newfilename);

          $sql = "INSERT INTO interests (title,image) VALUES ('$name','$newfilename')";

          $result = mysqli_query($conn, $sql);

          if($result){
            // header("location: ./?page=Front%20End");
            // echo "0";
          }else{
            // echo "1";
          }

        }
        
      }

    }
?>
<style type="text/css">
        .hide{display:none;}
        .btn {
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        }

    </style>
<!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <div class="box-header">
              <h3 class="box-title">Add Interests</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="form" action="../pages/Add Interest/addInterest.php" method="post" enctype="multipart/form-data">
                <div class="form-group" style="margin-left: 40%;">
                        <input type="file" name="image" id="imageUpload" class="hide"/> 
                         
                        <label for="imageUpload" class=""><img src="../pages/Add Interest/assets/image/user.png" style="height:100px;width:100px;border-radius:50px; border: 1px solid #007bff" id="imagePreview" alt="Preview Image" width="200px"/><br>Add Interests</label>

                </div>
                  <input type="text" class="form-control" name="title" placeholder="Interests Name"><br>
                  <br>
                  <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">ADD</button>
              </form>
            </div>
            <!-- /.box-body -->

            <div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $sqli = "SELECT * FROM interests";
                            $qryi = mysqli_query($conn,$sqli);

                 while($row = mysqli_fetch_array($qryi)){
                            $id = $row['id'];
                            $name= $row['title'];
                               
                             echo "<tr>
                          <td>$id</td>
                          <td>$name</td>
                          <td><a href='../pages/Add Interest/deleteInterest.php?id=$id'>Delete</a></td>
                          "; 
                          }
                          ?>
                </tbody>
              </table>
            </div>

          </div>

          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <script type="text/javascript" src="../pages/Add Interest/assets/js/jquery-3.3.1.min.js"></script>

    <script type="text/javascript">
         $('#imageUpload').change(function(){            
            readImgUrlAndPreview(this);
            function readImgUrlAndPreview(input){
                 if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {                          
                            $('#imagePreview').attr('src', e.target.result);
                            }
                      };
                      reader.readAsDataURL(input.files[0]);
                 }  
        });

    </script>