  <header class="main-header">
    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Backend</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label pull-right bg-red"><?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM redeem WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          echo $row["COUNT"];
                          $count = $row["COUNT"];
                      }
                  } else {
                      
                  }
                ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><?php echo $count;?> Redeem Requests.</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <?php 
                      $count = 0;
                        $sql = "SELECT v.f_name, v.l_name,v.id, r.userid, r.seen FROM viewers v, redeem r WHERE v.id = r.userid and r.seen = 0";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            
                            while($row = $result->fetch_assoc()) {
                              $client_id = $row['id'];
                                echo "<a href='../united/?page=Viewer%20Details&id=$client_id'><i class='fa fa-user text-red'></i>".$row['f_name']." ".$row['l_name']." requested to Redeem</a>";
                            }
                        } else {
                            echo "<a href='#'><i class='fa fa-user text-red'></i> No Requests</a>";
                        }
                      ?>
                  </li>
                  <!-- end message -->
                  
                </ul>
              </li>
              <li class="footer"><a href="../united/?page=user">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <?php 
                $count = 0;
                  $sql = "SELECT COUNT(*) AS COUNT FROM client WHERE seen = 0";
                  $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                          
                          $count = $row["COUNT"];
                      }
                  } else {
                      $count = 0;
                  }
                ?>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo $count;?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">
                <?php 
                  echo "You have " . $count. " unseen clients.";
                ?>
                </li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <!-- <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li> -->
                  <li>
                    <?php 
                      $count = 0;
                        $sql = "SELECT f_name,l_name,id FROM client WHERE seen = 0";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            
                            while($row = $result->fetch_assoc()) {
                              $client_id = $row['id'];
                                echo "<a href='../united/?page=Client%20Details&id=$client_id'><i class='fa fa-user text-red'></i>".$row['f_name']." ".$row['l_name']." requested to join Eads</a>";
                            }
                        } else {
                            echo "<a href='#'><i class='fa fa-user text-red'></i> No New Clients</a>";
                        }
                      ?>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="../united/?page=New%20Client">View all</a></li>
            </ul>
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php echo '<img src="'.$aimage.'" class="img-circle" style="height:20px;width:20px;" alt="User Image">'; ?>
              <span class="hidden-xs"><?php echo $aemail;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php echo '<img src="'.$aimage.'" class="img-circle" style="height:90px;width:90px;" alt="User Image">'; ?>
                <p>
                  <?php echo $aemail;?> <br> Admin
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="../login/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
