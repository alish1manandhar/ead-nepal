<?php
	header("Content-Type:application/json");
	 include("./authentication.php");
	 include_once('../../connection.php');
	 if(!empty($_POST['admin_token']) || !empty($_POST['id'])) {
	 	  $admin_token = $_POST['admin_token'];
	 	  $id = $_POST['id'];

	      $admin_token_id = mysqli_real_escape_string($conn,$_POST['admin_token']);
	      $ids = mysqli_real_escape_string($conn,$_POST['id']);

	 		$login = authentication($admin_token_id,$ids);
	        
	 	if(empty($login)){
	 		deliver_response(200,"Something Went Wrong! not Admin?",NULL);
	 	}else{
	 		deliver_response(200,"Success",$login);
	 	}
	 	
	 }else{
		//throws invalid request
	 		deliver_response(400,"invalid login","null");
	 }

	 function deliver_response($status,$status_message,$data){
	 	header("HTTP/1.1 $status $status_message");

	 	$response['status'] = $status;
	 	$response['status_message'] = $status_message;
	 	$response['data'] = $data;

	 	$json_response = json_encode($response);
	 	echo stripslashes($json_response);
 	 }
?>