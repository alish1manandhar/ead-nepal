-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2018 at 09:19 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eads`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_ads`
--

CREATE TABLE `add_ads` (
  `id` int(11) NOT NULL,
  `a_title` varchar(100) NOT NULL,
  `target_gender` varchar(50) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `age_group` varchar(50) NOT NULL,
  `interest` varchar(1000) NOT NULL,
  `doc_image` varchar(600) NOT NULL,
  `added_date` date NOT NULL,
  `reach_out_price` float NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `fk_client_id` int(11) NOT NULL,
  `initial_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_ads`
--

INSERT INTO `add_ads` (`id`, `a_title`, `target_gender`, `location`, `age_group`, `interest`, `doc_image`, `added_date`, `reach_out_price`, `type`, `status`, `fk_client_id`, `initial_price`) VALUES
(1, 'Production project assignment', 'Female', 'Asdfas', '15,30', 'Asdf', 'Download-Leo-Transparent-Background.png', '2018-12-10', 200, 'img', 1, 2, 1000),
(2, 'Production project assignment', 'Male', 'Asdsf', '15,51', 'Asdf', 'logo1.png', '2018-11-18', 50, 'img', 0, 1, 111),
(4, 'Production project assignment', 'all', 'Asdfas', '15,30', 'Asdfas', 'logo5.png', '2018-11-13', 5, 'img', 1, 1, 50),
(6, 'Production project assignment', 'Male', 'Asdsf', '15,30', 'Asdf', 'banner_for_football.png', '2018-10-01', 10, 'img', 1, 1, 100),
(7, 'Production project assignment', 'all', 'Asdfas', '15,74', 'Asdf', 'Download-Leo-Transparent-Background.png', '2018-11-01', 4, 'img', 1, 1, 100),
(8, 'Production project assignment', 'all', 'Arghakhanchi', '15,30', 'Asdf', '18446552.jpg', '2018-12-04', 654, 'img', 1, 1, 1000),
(9, 'Gorkha Panel', 'all', 'Baglung', '15,30', 'Asdfas', 'fit.png', '2018-12-04', 56, 'img', 1, 1, 123),
(10, 'Production project assignment', 'all', 'Achham', '15,30', 'Asdfas', '1522983979.png', '2018-12-04', 72.4, 'img', 1, 1, 100),
(11, 'Production project assignment', 'all', 'Arghakhanchi', '15,30', 'Asdf', 'banner_for_football.png', '2018-12-04', 300.3, 'img', 1, 1, 4000),
(12, 'Buy products', 'all', 'Arghakhanchi', '15,30', 'Asdfas', '115753977f57178.jpg', '2018-10-04', 50, 'img', 1, 1, 1000),
(13, 'Football', 'all', 'Bara', '15,30', 'Asdsf', 'banner_for_football_final.jpg', '2018-12-04', 50, 'img', 1, 1, 50),
(14, 'Internet Package', 'all', 'Achham', '15,30', 'Asdsf', 'vianet.png', '2018-12-04', 50, 'img', 0, 1, 233),
(15, 'Lions logo', 'all', 'Arghakhanchi', '15,30', 'Asdsf', 'Download-Leo-Transparent-Background.png', '2018-12-04', 50, 'img', 0, 1, 100),
(16, 'Daraj Logo', 'all', 'Arghakhanchi', '15,30', 'Asdfas', 'Daraz_logo_color.png', '2018-09-04', 50, 'img', 1, 1, 54),
(17, 'Icon', 'all', 'Arghakhanchi', '15,30', 'Asdf', 'Daraz_logo_color.png', '2018-09-04', 50, 'img', 0, 1, 100),
(18, 'Production project assignment', 'all', 'Baglung', '15,30', 'Asdf', 'Daraz_logo_color.png', '2018-12-04', 50, 'img', 1, 1, 100),
(19, 'Production project assignment', 'all', 'Achham', '15,30', 'Asdfas', '18446552.jpg', '2018-04-04', 50, 'img', 0, 1, 100),
(20, 'Sadf', 'all', 'Baglung', '15,30', 'Asdf', 'celebration-confetti-colorful-ribbons-luxury-greeting-rich-card_29865-450.jpg', '2018-12-04', 50, 'img', 1, 1, 100),
(21, 'Sadf', 'all', 'Baglung', '15,30', 'Asdf', 'celebration-confetti-colorful-ribbons-luxury-greeting-rich-card_29865-450.jpg', '2018-03-04', 50, 'img', 0, 1, 100),
(22, 'Sadf', 'all', 'Baglung', '15,30', 'Asdf', 'celebration-confetti-colorful-ribbons-luxury-greeting-rich-card_29865-450.jpg', '2018-02-04', 50, 'img', 1, 1, 100),
(23, 'Sadf', 'all', 'Baitadi', '15,30', 'Asdfas', '1522983979.png', '2018-12-04', 50, 'img', 0, 1, 100),
(24, 'Sadf', 'all', 'Baitadi', '15,30', 'Asdfas', '1522983979.png', '2018-12-04', 50, 'img', 1, 1, 100),
(25, 'Sadf', 'all', 'Baitadi', '15,30', 'Asdfas', '1522983979.png', '2018-12-04', 50, 'img', 0, 1, 100),
(26, 'Sadf', 'all', 'Baglung', '15,30', 'Asdsf', 'Daraz_logo_color.png', '2018-12-04', 50, 'img', 1, 1, 100),
(27, 'Sadf', 'all', 'Baglung', '15,30', 'Asdf', '042938779aaac00dd96ac6e162f94289.png', '2018-10-04', 48, 'img', 0, 1, 100),
(28, 'Production project assignment', 'all', 'Arghakhanchi', '15,30', 'Asdfas', 'logo3.png', '2018-12-04', 50, 'img', 1, 1, 232),
(29, 'Production project assignment', 'all', 'Arghakhanchi', '15,30', 'Asdfas', 'logo5.png', '2018-12-04', 75.3, 'img', 0, 1, 500),
(30, 'Design', 'all', 'Baglung', '15,30', 'Asdf', 'low_poly_texture_by_zappyspiker-d8sjuhm.png', '2018-01-05', 50, '', 0, 1, 654),
(31, 'Production project assignment', 'all', 'Baitadi', '15,30', 'Asdf', 'empty-football-field-arena-stadium-vector-21269013.jpg', '2018-01-07', 50, '', 0, 1, 122),
(32, 'Production project assignment', 'all', 'Baglung', '15,30', 'Asdfas', '18446552.jpg', '2018-01-07', 50, '', 0, 1, 100),
(33, 'Logo design', 'all', 'Banke', '27,37', 'Asdsf', 'Download-Leo-Transparent-Background.png', '2018-12-07', 109.5, '', 0, 1, 500),
(34, 'Aaaaaaaaaaaaaaaaaaaaaaaa', 'all', 'Baglung', '15,30', 'Asdf', 'Download-Leo-Transparent-Background.png', '2018-01-08', 50, '', 0, 1, 233),
(35, 'Futsal', 'all', 'Bajhang', '15,30', 'Asdfas', 'Untitled-1.jpg', '2018-12-22', 50, '', 1, 1, 50),
(36, 'Football', 'all', 'Baglung', '15,30', 'Asdfas', 'Vecteezy-050718-13940-29.jpg', '2018-12-22', 49, '', 0, 1, 50);

-- --------------------------------------------------------

--
-- Table structure for table `add_audio_ads`
--

CREATE TABLE `add_audio_ads` (
  `id` int(11) NOT NULL,
  `a_title` varchar(100) NOT NULL,
  `target_gender` varchar(50) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `age_group` varchar(50) NOT NULL,
  `interest` varchar(1000) NOT NULL,
  `audio` varchar(600) NOT NULL,
  `added_date` date NOT NULL,
  `reach_out_price` float NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `fk_client_id` int(11) NOT NULL,
  `initial_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_audio_ads`
--

INSERT INTO `add_audio_ads` (`id`, `a_title`, `target_gender`, `location`, `age_group`, `interest`, `audio`, `added_date`, `reach_out_price`, `type`, `status`, `fk_client_id`, `initial_price`) VALUES
(35, 'Audio gnment', 'all', 'Achham', '15,30', 'Asdf', 'Recording.mp3', '2018-12-08', 49, 'audio', 0, 1, 100),
(36, 'Production project assignment', 'all', 'Bhaktapur', '15,30', 'Asdfas', 'Recording.mp3', '2018-12-10', 48, 'audio', 1, 1, 50),
(37, 'Audio gnment 1', 'all', 'Achham', '15,30', 'Asdf', 'Recording.mp3', '2018-12-08', 49, 'audio', 1, 1, 60),
(38, 'Production project 2', 'all', 'Bhaktapur', '15,30', 'Asdfas', 'Recording.mp3', '2018-12-10', 50, 'audio', 1, 1, 122),
(39, 'Audio gnment 3', 'all', 'Achham', '15,30', 'Asdf', 'Recording.mp3', '2018-12-08', 49, 'audio', 1, 1, 1000),
(40, 'Production project 4', 'all', 'Bhaktapur', '15,30', 'Asdfas', 'Recording.mp3', '2018-12-10', 50, 'audio', 1, 1, 70),
(41, 'Audio gnment 5', 'all', 'Achham', '15,30', 'Asdf', 'Recording.mp3', '2018-12-08', 49, 'audio', 1, 1, 50),
(42, 'Production project 6', 'all', 'Bhaktapur', '15,30', 'Asdfas', 'Recording.mp3', '2018-12-10', 50, 'audio', 1, 1, 80);

-- --------------------------------------------------------

--
-- Table structure for table `add_video_ads`
--

CREATE TABLE `add_video_ads` (
  `id` int(11) NOT NULL,
  `a_title` varchar(100) NOT NULL,
  `target_gender` varchar(50) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `age_group` varchar(50) NOT NULL,
  `interest` varchar(1000) NOT NULL,
  `video` varchar(600) NOT NULL,
  `added_date` date NOT NULL,
  `reach_out_price` float NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `fk_client_id` int(11) NOT NULL,
  `initial_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_video_ads`
--

INSERT INTO `add_video_ads` (`id`, `a_title`, `target_gender`, `location`, `age_group`, `interest`, `video`, `added_date`, `reach_out_price`, `type`, `status`, `fk_client_id`, `initial_price`) VALUES
(36, 'Production project assignment', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 48, 'video', 1, 1, 48),
(37, 'Production project assignment 1', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 500),
(38, 'Production project assignment2', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 100),
(39, 'Production project assignment 3', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 500),
(40, 'Production project assignment 4', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 500),
(41, 'Production project assignment 5', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 1000),
(42, 'Production project assignment 6', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 50, 'video', 1, 1, 100),
(43, 'Production project assignment 7', 'all', 'Bara', '15,30', 'Asdfas', 'Dota 2 3_20_2018 7_50_39 PM.mp4', '2018-12-11', 49, 'video', 1, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(100) NOT NULL,
  `id` int(11) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`email`, `id`, `password`, `status`) VALUES
('admin@admin.com', 1, '5f4dcc3b5aa765d61d8327deb882cf99', 1);

-- --------------------------------------------------------

--
-- Table structure for table `audioviews`
--

CREATE TABLE `audioviews` (
  `id` int(11) NOT NULL,
  `fk_viewer` int(11) NOT NULL,
  `fk_ads` int(11) NOT NULL,
  `watch_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audioviews`
--

INSERT INTO `audioviews` (`id`, `fk_viewer`, `fk_ads`, `watch_date`) VALUES
(1, 1, 35, '2018-12-22'),
(2, 1, 36, '2018-12-22'),
(3, 1, 36, '2018-12-22');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `f_name` varchar(60) NOT NULL,
  `l_name` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_type` varchar(100) NOT NULL,
  `company_pan` varchar(100) NOT NULL,
  `est_date` date NOT NULL,
  `id` int(11) NOT NULL,
  `password` varchar(250) NOT NULL,
  `doc_image` varchar(600) NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`f_name`, `l_name`, `email`, `phone`, `company_name`, `company_type`, `company_pan`, `est_date`, `id`, `password`, `doc_image`, `balance`) VALUES
('client', 'client', 'client@client.com', '9852431658', 'ads cpmpany', 'multimedia', '1254785632', '2018-11-01', 1, '5f4dcc3b5aa765d61d8327deb882cf99', 'logo5.png', 113.2),
('Alish', 'Manandhar', 'Maskofnepal@gmail.com', '9843169524', 'Future Tech', 'Marketing Company', '6589541257', '2018-12-19', 2, '5f4dcc3b5aa765d61d8327deb882cf99', 'logo5.png', 100.12000274658203),
('Alisha', 'Manandhar', 'alish1manandhar@gmail.com', '9843169524', 'Future Tech', 'Marketing Company', '6589541257', '2018-12-27', 3, '5f4dcc3b5aa765d61d8327deb882cf99', 'logo1.png', 950);

-- --------------------------------------------------------

--
-- Table structure for table `imageviews`
--

CREATE TABLE `imageviews` (
  `id` int(11) NOT NULL,
  `fk_viewer` int(11) NOT NULL,
  `fk_ads` int(11) NOT NULL,
  `watch_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imageviews`
--

INSERT INTO `imageviews` (`id`, `fk_viewer`, `fk_ads`, `watch_date`) VALUES
(1, 1, 11, '2018-12-21'),
(2, 1, 8, '2018-12-01'),
(3, 1, 8, '2018-12-09'),
(4, 1, 30, '2018-12-21'),
(5, 1, 19, '2018-12-21'),
(6, 1, 14, '2018-12-21'),
(7, 1, 8, '2018-12-22'),
(8, 1, 30, '2017-01-21'),
(9, 1, 27, '2018-11-21'),
(10, 1, 27, '2018-12-21'),
(11, 1, 7, '2018-12-21'),
(12, 1, 33, '2018-12-22'),
(13, 2, 8, '2018-12-22'),
(14, 1, 36, '2018-12-23');

-- --------------------------------------------------------

--
-- Table structure for table `videoviews`
--

CREATE TABLE `videoviews` (
  `id` int(11) NOT NULL,
  `fk_viewer` int(11) NOT NULL,
  `fk_ads` int(11) NOT NULL,
  `watch_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videoviews`
--

INSERT INTO `videoviews` (`id`, `fk_viewer`, `fk_ads`, `watch_date`) VALUES
(1, 1, 40, '2018-12-22'),
(2, 2, 40, '2018-12-22'),
(3, 1, 43, '2018-12-23');

-- --------------------------------------------------------

--
-- Table structure for table `viewers`
--

CREATE TABLE `viewers` (
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `location` varchar(100) NOT NULL,
  `college` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  `field_of_study` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `post` varchar(100) NOT NULL,
  `interest` varchar(500) NOT NULL,
  `id` int(11) NOT NULL,
  `reg_date` date NOT NULL,
  `password` varchar(200) NOT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `viewers`
--

INSERT INTO `viewers` (`f_name`, `l_name`, `email`, `phone`, `location`, `college`, `level`, `field_of_study`, `company`, `post`, `interest`, `id`, `reg_date`, `password`, `balance`, `status`) VALUES
('alish', 'manandhar', 'alish1manandhar@gmail.com', '23223210', 'Bhaktapur', 'Khwopa', 'it', 'it', 'sometthing', 'manager', 'interest1, interest2', 1, '2018-12-11', '5f4dcc3b5aa765d61d8327deb882cf99', 14, 1),
('alisha', 'manandhar', 'alisha1manandhar@gmail.com', '23223210', 'Janakpur', 'Khwopa', 'it', 'it', 'sometthing', 'manager', 'interest1, interest2', 2, '2018-12-11', '5f4dcc3b5aa765d61d8327deb882cf99', 10, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_ads`
--
ALTER TABLE `add_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_audio_ads`
--
ALTER TABLE `add_audio_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_video_ads`
--
ALTER TABLE `add_video_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audioviews`
--
ALTER TABLE `audioviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imageviews`
--
ALTER TABLE `imageviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videoviews`
--
ALTER TABLE `videoviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `viewers`
--
ALTER TABLE `viewers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_ads`
--
ALTER TABLE `add_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `add_audio_ads`
--
ALTER TABLE `add_audio_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `add_video_ads`
--
ALTER TABLE `add_video_ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `audioviews`
--
ALTER TABLE `audioviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imageviews`
--
ALTER TABLE `imageviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `videoviews`
--
ALTER TABLE `videoviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `viewers`
--
ALTER TABLE `viewers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
