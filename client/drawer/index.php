<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <?php echo '<img src="'.$aimage.'" class="img-circle" style="height:45px;width:45px;" alt="User Image">'; ?>
        </div>
        <div class="pull-left info">
          <p><?php echo $aemail;?></p>
          <a href="#" style="font-size:25px"><b>Nrs. </b><?php echo $money;?> </a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="./?page=dashboard"><i class="fa fa-home"></i> Dashboard</a></li>
        <li><a href="./?page=Important Details"><i class="fa fa-user"></i> Important Details</a></li>
        <li><a href="./?page=Balance Details"><i class="fa fa-user"></i> Balance Details</a></li>
        <li><a href="./?page=Personal Information"><i class="fa fa-user"></i> Personal Information</a></li>
        <li  class="treeview">
          <a href="#">
            <i class="fa fa-photo"></i> <span>Add Ads</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="./?page=Select Image"><i class="fa fa-circle-o"></i>Add Image Ads</a></li>
            <!-- <li><a href="./?page=Live Image Ads"><i class="fa fa-circle-o"></i>My Live Image Ads</a></li> -->
            <li><a href="./?page=target audio"><i class="fa fa-circle-o"></i>Add Audio Ads</a></li>
            <!-- <li><a href="./?page=Live Audio Ads"><i class="fa fa-circle-o"></i>My Live Audio Ads</a></li> -->
            <li><a href="./?page=target video"><i class="fa fa-circle-o"></i>Add Video Ads</a></li>
            <!-- <li><a href="./?page=Live Video Ads"><i class="fa fa-newspaper-o"></i>My Live Video Ads</a></li> -->
          </ul>
        </li>
        <li><a href="./?page=Insight"><i class="fa fa-newspaper-o"></i>Insight</a></li>
        <li><a href="./?page=gallery"><i class="fa fa-newspaper-o"></i>Gallery</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>