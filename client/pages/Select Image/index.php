<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title"><a href="./?page=Upload Image">Add New Image Here <i class="fa fa-hand-pointer-o"></i></a></h3><br>
              <h3 class="box-title bg-danger">NOTE: Please Upload Image from gallery.</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <?php 
                if (isset($_SESSION['upload'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['upload']; ?>
                    </div>
                  <?php
                  unset($_SESSION['upload']);
                }elseif(isset($_SESSION['uploadErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['uploadErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['uploadErr']);

                }
              ?>    
                      <div class="row">
                        <?php 
                          $images = "SELECT * FROM gallery_images where fk_client_id = $aid";
                          $imagesResult = mysqli_query($conn, $images);

                              
                              while($row = mysqli_fetch_assoc($imagesResult)) {
                                $img = $row['image_name'];
                                      ?>
                                      <div class="col-md-3"  style="text-align:center;">
                                        <img style="width:100%;height:200px;display: block;object-fit:cover;" src="<?php echo '../pages/target/uploads/'.$img; ?>"><br>
                                        <a  type="button" class="btn btn-success" href="<?php echo './?page=target&img_id='.$img; ?>">Select This <i class="fa fa-hand-pointer-o"></i></a>
                                      </div>
                                      <?php
                              }
                        ?>
                      </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
