<section class="content">
      <div class="row">
        
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">All Live Image Ads</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <?php 
                if (isset($_SESSION['upload'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['upload']; ?>
                    </div>
                  <?php
                  unset($_SESSION['upload']);
                }elseif(isset($_SESSION['uploadErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['uploadErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['uploadErr']);

                }
              ?>    
                
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Audio</th>
                  <th>Added Date</th>
                  <th>Price</th>
                  <th>Details</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM add_audio_ads where fk_client_id = $aid AND status = 1 AND reach_out_price > 0";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          $status = "";
                          // $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='height:50px;'/>";
                            
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td><audio controls src='../pages/target audio/uploads/".$row['audio']."' type='audio/mpeg' ></audio></td>
                              <td>".$row['added_date']."</td>
                              <td>".$row['reach_out_price']."</td>
                              <td><a href='#'><i class='fa fa-hand-pointer-o'></i>Details</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>