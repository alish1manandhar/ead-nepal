<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<section class="content">
      <div class="row">
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">History of added Ads</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav active">
                  <a class="nav-link active" id="image_tab" data-toggle="tab" href="#Image" role="tab" aria-controls="Image" aria-selected="true">Image</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Audio-tab" data-toggle="tab" href="#Audio" role="tab" aria-controls="Audio" aria-selected="false">Audio</a>
                </li>
                <li class="nav">
                  <a class="nav-link" id="Video-tab" data-toggle="tab" href="#Video" role="tab" aria-controls="Video" aria-selected="false">Video</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  in active " id="Image" role="tabpanel" aria-labelledby="image_tab">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Added Date</th>
                      <th>Insights</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?php 
                        $sql="SELECT * FROM add_ads where fk_client_id = $aid ";
                        $result=mysqli_query($conn,$sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $img = "<img alt='e-ads' src='../pages/target/uploads/".$row['doc_image']."' style='width:150px;'/>";
                                echo "
                                  <tr>
                                    <td>".$row['id']."</td>
                                    <td>".$row['a_title']."</td>
                                    <td>".$row['added_date']."</td>
                                    <td><a href='./?page=Detailed Image Insights&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                                  </tr>
                                ";
                            }
                        } else {
                            echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                        }
                      ?>  
                      </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="Audio" role="tabpanel" aria-labelledby="Audio-tab">
                  <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Insights</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $sql="SELECT * FROM add_audio_ads where fk_client_id = $aid";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td><a href='./?page=Detailed Audio Insights&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
                <div class="tab-pane fade" id="Video" role="tabpanel" aria-labelledby="Video-tab">
                  <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Added Date</th>
                  <th>Insights</th>
                </tr>
                </thead>
                <tbody>

                <?php 
                  $sql="SELECT * FROM add_video_ads where fk_client_id = $aid";
                  $result=mysqli_query($conn,$sql);
                  if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) {
                          echo "
                            <tr>
                              <td>".$row['id']."</td>
                              <td>".$row['a_title']."</td>
                              <td>".$row['added_date']."</td>
                              <td><a href='./?page=Detailed Video Insights&pid=".$row['id']."'><i class='fa fa-hand-pointer-o'></i>Insights</a></td>
                            </tr>
                          ";
                      }
                  } else {
                      echo "<tr><td></td><td></td><td>No Data</td><td> X</td><td>X</td><td>X</td></tr>";
                  }
                ?>  
                </tbody>
              </table>
                </div>
              </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
