<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.css'>

      <link rel="stylesheet" href="../pages/target/css/style.css">    
    <section class="content">
      <div class="row">
        
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Upload Image here</h3><br>
              <h3 class="box-title"><a href="#" onclick="window.history.go(-1); return false;">Go Back</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <?php 
                if (isset($_SESSION['upload'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['upload']; ?>
                    </div>
                  <?php
                  unset($_SESSION['upload']);
                }elseif(isset($_SESSION['uploadErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['uploadErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['uploadErr']);

                }
              ?>    
                <form class="dropzone" action="../pages/target/upload.php" method="post" enctype="multipart/form-data">
                  <div class="col-xs-12">
                    <div class="form-group">
                      <main class="page">
                        <h2>Upload ,Crop and save.</h2>
                        <!-- input file -->
                        <div class="box">
                          <!-- <input type="file" id="file-input" value="" accept="image/*"> -->
                          <input type="file" name="file-input" id="file-input" onchange="uploadFile()" accept="image/*"><br>
                          <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                          <h3 id="status"></h3>
                          <p id="loaded_n_total"></p>
                        </div>
                        <!-- leftbox -->
                        <div class="box-2">
                          <div class="result"></div>
                        </div>
                        <!--rightbox-->
                        <div class="box-2 img-result hide">
                          <!-- result of crop -->
                          <img class="cropped" src="" alt="">
                        </div>
                        <!-- input file -->
                        <div class="box">
                          <div class="options hide">
                            <label> Width</label>
                            <input type="number"  class="img-w" value="1080" min="100" max="1200" />
                          </div>
                          <!-- save btn -->
                          <button class="btn save hide">Save</button>
                          <!-- download btn -->
                          <a href="" class="btn download  hide">Download</a>
                        </div>
                      </main>
                    </div>
                  </div>
                  
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

      <script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>

  

    <script  src="../pages/target/js/index.js"></script>
    <script type="text/javascript">
      function _(el) {
  return document.getElementById(el);
}

function uploadFile() {
  var file = _("file-input").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file-input", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", "../pages/target audio/loadaudio.php"); // http://www.developphp.com/video/JavaScript/File-Upload-Progress-Bar-Meter-Tutorial-Ajax-PHP
  //use file_upload_parser.php from above url
  ajax.send(formdata);
}

function progressHandler(event) {
  _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
}

function completeHandler(event) {
  _("status").innerHTML = event.target.responseText;
  _("progressBar").value = 0; //wil clear progress bar after successful upload
}

function errorHandler(event) {
  _("status").innerHTML = "Upload Failed";
}

function abortHandler(event) {
  _("status").innerHTML = "Upload Aborted";
}
    </script>