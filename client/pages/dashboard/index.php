    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php include 'photoCount.php';?></h3>

              <p>Image Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-photo-o"></i>
            </div>
            <a href="./?page=Insight" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php include 'videoCount.php';?></h3>

              <p>Video Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-video-o"></i>
            </div>
            <a href="./?page=History" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php include 'audioCount.php';?></h3>

              <p>Audio Ads</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-audio-o"></i>
            </div>
            <a href="./?page=History" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php include 'mybalance.php';?></h3>

              <p>My Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="./?page=History" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
 