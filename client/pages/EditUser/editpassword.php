<?php 
  include_once './header.php';

  $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
        $viewerDetail = mysqli_query($conn, $viewer);

        if (mysqli_num_rows($viewerDetail) > 0) {
            // output data of each row
            $row = mysqli_fetch_assoc($viewerDetail);
            
        }else{
          header('location: ../user_login.php');
        }
?>

<?php 
if (isset($_POST['submit'])) {
      $oldpass = md5(mysqli_real_escape_string($conn,$_POST['oldpass']));
      $newpass = mysqli_real_escape_string($conn,$_POST['newpass']);
      $confirm = mysqli_real_escape_string($conn,$_POST['confirm']);

        if (!($row['password'] == $oldpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Old Password Dont Match.</strong></div>';
        }else if(!($newpass == $confirm)){
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>New Password and Confirm Password Dont Match.</strong></div>';
        }elseif (strlen($newpass) < 9) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Length Must Be Greater Than 9</strong></div>';
        }elseif (!preg_match("#[0-9]+#",$newpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain A Number</strong></div>';
        }elseif(!preg_match("#[A-Z]+#",$newpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Capital Alphabets</strong></div>';
        }elseif (!preg_match("#[a-z]+#",$newpass)){
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Small Alphabets</strong></div>';
        }else{
          $newPassword = md5($newpass);
           $update = "UPDATE viewers SET 
           password = '".$newPassword."'
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              $_SESSION['PasswordSuccess'] = '<div class="alert alert-success"><strong>Password Changed.</strong></div>';
              header("Location: ../user/userpage.php");
            }else{
              $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Update Failed.</strong></div>';
            }
        }
}else{

}


?>
 <style type="text/css">
  html, body{
    height: 100%;
  }
     @import url('https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Rubik:300,400,500,700,900');
        h2,
          h3 {
            orphans: 3;
            widows: 3;
          }

          h1, h2, h4, h5 {
          font-family: "Open Sans", Arial, sans-serif;

        }
        p{
          color: white;
        }
        body{
          color: white;
        }
     #ad_category{
            margin-top:50px;
            }
         


label{
  margin-bottom: 10px;
}
</style>


  <div class="container category_content"  id="ad_category">
        <!-- /.row -->
        <div class="row">
            
            <!-- /.col -->
            <div class="col-md-12">
                

                <div class="row">

               
                <div class="col-sm-8 col-md-8">
                     <div class="contact_form">
                    <form action="" method="post" enctype="multipart/form-data">
                    
                    <div class="row">
                       <?php 
                          if (isset($_SESSION['PasswordError'])) {
                            echo $_SESSION['PasswordError'];
                            unset($_SESSION['PasswordError']);
                          }
                        
                        ?>
                        <div class=" col-md-4 "> 
                            <div class="">
                                <label>Old Password</label>
                                <input name="oldpass" type="password" class="form-control" placeholder="Old Password" required>
                            </div>
                        </div>
                        <div class=" col-md-4 "> 
                            <div class="">
                                <label>New Password</label>
                                <input  name="newpass" type="password" class="form-control" placeholder="NewPassword" required>
                            </div>
                        </div>
                        <div class=" col-md-4 "> 
                            <div class="">
                                <label>Confirm Password</label>
                                <input  name="confirm" type="password" class="form-control" placeholder="Confirm Password" required>
                            </div>
                        </div>
                    </div>
                
                    <div class=" text-right">
                        <button type="Submit" name="submit" class="btn btn-rounded btn-primary">Change</button>
                    </div>
                <form>
                </div>
                    
                </div>
                <div class="col-sm-2 col-md-2">
                    
                </div>
                </div>
                
               
                
                <!-- /.row -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

<?php 
  include_once './footer.php';
?>