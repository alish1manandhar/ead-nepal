    <!-- Main content -->
    <!-- Main content -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.2/css/bootstrap-slider.css" integrity="sha256-iqTB1uUz7O/0QzOYSmeecY5jdbfGB63rGaDrr6AorWk=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.2/css/bootstrap-slider.min.css" integrity="sha256-G1KdbpcBqOPS1CM2lArzU9Bp5l1ThGmaGe3A94ohHDg=" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.2/bootstrap-slider.js" integrity="sha256-hH1mjMALlbbd8i6Qq58XV36OA0/kYG33bPL0wCAY1Z4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.3.2/bootstrap-slider.min.js" integrity="sha256-4LIfapY8qfO4zanc6lv5duDuCtxOMgDwKdn51JuFVzA=" crossorigin="anonymous"></script>
    <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <style type="text/css">
      .slider-handle{
        background-image:linear-gradient(to bottom,#3F51B5 0,#3F51B5 100%);
      }
      .navbar-nav {
        flex-direction: row;
      }
      .filter-option{
        background: white;
        border: 1px solid lightgray;
      }
      .show-tick{
        width: 100%!important;
      }

      .file-upload {
        background-color: #ffffff;
        width: 100%;
        margin: 0 auto;
        padding: 20px;
      }

      .file-upload-btn {
        width: 100%;
        margin: 0;
        color: #fff;
        background: #008bd0;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #039be6;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
      }

      .file-upload-btn:hover {
        background: #039be6;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
      }

      .file-upload-btn:active {
        border: 0;
        transition: all .2s ease;
      }

      .file-upload-content {
        display: none;
        text-align: center;
      }

      .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        outline: none;
        opacity: 0;
        cursor: pointer;
      }

      .image-upload-wrap {
        margin-top: 20px;
        border: 4px dashed #008bd0;
        position: relative;
      }

      .image-dropping,
      .image-upload-wrap:hover {
        background-color: #008bd0;
        border: 4px dashed #ffffff;
      }

      .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
      }

      .drag-text {
        text-align: center;
      }

      .drag-text h3 {
        font-weight: 100;
        text-transform: uppercase;
        color: #15824B;
        padding: 60px 0;
      }

      .file-upload-image {
        max-height: 200px;
        max-width: 100%;
        margin: auto;
      }

      .remove-image {
        width: 200px;
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
      }

      .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
      }

      .remove-image:active {
        border: 0;
        transition: all .2s ease;
      }

       /*slider from here*/
      .slidecontainer {
          width: 100%;
      }

      .slider,.slider1 {
          -webkit-appearance: none;
          width: 100%;
          height: 25px;
          background: #dee2e4;
          outline: none;
          opacity: 0.7;
          -webkit-transition: .2s;
          transition: opacity .2s;
      }

      .slider:hover,.slider1:hover {
          opacity: 1;
      }

      .slider::-webkit-slider-thumb ,.slider1::-webkit-slider-thumb{
          -webkit-appearance: none;
          appearance: none;
          width: 25px;
          height: 25px;
          background: #3F51B5;
          cursor: pointer;
      }

      .slider::-moz-range-thumb,.slider1::-moz-range-thumb {
          width: 25px;
          height: 25px;
          background: #3F51B5;
          cursor: pointer;
      }
    </style>

    <section class="content">
      <div class="row">
        
          <div class="box" >
            <div class="box-header with-border">
              <h3 class="box-title">Add New Ads here</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

             <?php 
                if (isset($_SESSION['upload'])) {
                  ?>
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> Success!</h4>
                      <?php echo $_SESSION['upload']; ?>
                    </div>
                  <?php
                  unset($_SESSION['upload']);
                }elseif(isset($_SESSION['uploadErr'])) {
                  ?>
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                      <?php echo $_SESSION['uploadErr']; ?>
                    </div>
                  <?php
                  unset($_SESSION['uploadErr']);

                }
              ?>    
                <form class="dropzone" action="../pages/target audio/upload.php" method="post" enctype="multipart/form-data">
                  <div class="col-xs-6">
                    <div class="form-group">
                      <div class="file-upload">
                      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Audio</button>
                        <input class="file-upload-input" onchange="uploadFile()" accept=".mp3,.m4a,.ogg,.wav" name="image" type='file' onchange="readURL(this);" id="file1" required/>
                      </div>
                      <p ><b>Only mp3, m4a, ogg or .wav files are supported!<br>
                        You cannot upload audio of duration greater than 30 sec!
                      </b></p>
                      <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
                      <h3 id="status"></h3>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="des" class="form-control" placeholder="Ads description" ></textarea>
                    </div>
                    <div class="form-group">
                        <label>Email (if any!)</label>
                        <input type="email" name="email" class="form-control" placeholder="email">
                    </div>
                    <div class="form-group">
                        <label>phone number (if any!)</label>
                        <input type="number" name="phone" class="form-control" placeholder="phone number">
                    </div>
                    <label>Website (if any!)</label>
                    <div class="input-group">
                      <span class="input-group-addon">https://</span>  
                      <input type="text" name="url" class="form-control" placeholder="www.abc.com">
                    </div>
                  </div>
                  <div class="col-xs-6">

                    <div class="form-group">
                        <label>ADS TITLE</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter Banner Title" required>
                    </div>
                    <!-- radio -->
                    <div class="form-group">
                      <label>GENDER <br> MALE :
                        <input type="checkbox" value="Male" name="gender" class="minimal-red" checked>
                      </label>
                      <label>&nbsp &nbsp FEMALE :
                        <input type="checkbox" value="Female" name="gender1" class="minimal-red" checked>
                      </label>
                    </div>

                    
                    <div class="form-group" >
                      <label>LOCATION </label> <br>
                      <select name ="location[]" class="selectpicker" multiple data-live-search="true" required >
                        <option value="all">All</option>
                        <option value="Achham">Achham</option>
                        <option value="Arghakhanchi">Arghakhanchi</option>
                        <option value="Baglung">Baglung</option>
                        <option value="Baitadi">Baitadi</option>
                        <option value="Bajhang">Bajhang</option>
                        <option value="Bajura">Bajura</option>
                        <option value="Banke">Banke</option>
                        <option value="Bara">Bara</option>
                        <option value="Bardiya">Bardiya</option>
                        <option value="Bhaktapur">Bhaktapur</option>
                        <option value="Bhojpur">Bhojpur</option>
                        <option value="Chitwan">Chitwan</option>
                        <option value="Dadeldhura">Dadeldhura</option>
                        <option value="Dailekh">Dailekh</option>
                        <option value="Dang deukhuri">Dang deukhuri</option>
                        <option value="Darchula">Darchula</option>
                        <option value="Dhading">Dhading</option>
                        <option value="Dhankuta">Dhankuta</option>
                        <option value="Dhanusa">Dhanusa</option>
                        <option value="Dholkha">Dholkha</option>
                        <option value="Dolpa">Dolpa</option>
                        <option value="Doti">Doti</option>
                        <option value="Gorkha">Gorkha</option>
                        <option value="Gulmi">Gulmi</option>
                        <option value="Humla">Humla</option>
                        <option value="Ilam">Ilam</option>
                        <option value="Jajarkot">Jajarkot</option>
                        <option value="Jhapa">Jhapa</option>
                        <option value="Jumla">Jumla</option>
                        <option value="Kailali">Kailali</option>
                        <option value="Kalikot">Kalikot</option>
                        <option value="Kanchanpur">Kanchanpur</option>
                        <option value="Kapilvastu">Kapilvastu</option>
                        <option value="Kaski">Kaski</option>
                        <option value="Kathmandu">Kathmandu</option>
                        <option value="Kavrepalanchok">Kavrepalanchok</option>
                        <option value="Khotang">Khotang</option>
                        <option value="Lalitpur">Lalitpur</option>
                        <option value="Lamjung">Lamjung</option>
                        <option value="Mahottari">Mahottari</option>
                        <option value="Makwanpur">Makwanpur</option>
                        <option value="Manang">Manang</option>
                        <option value="Morang">Morang</option>
                        <option value="Mugu">Mugu</option>
                        <option value="Mustang">Mustang</option>
                        <option value="Myagdi">Myagdi</option>
                        <option value="Nawalparasi">Nawalparasi</option>
                        <option value="Nuwakot">Nuwakot</option>
                        <option value="Okhaldhunga">Okhaldhunga</option>
                        <option value="Palpa">Palpa</option>
                        <option value="Panchthar">Panchthar</option>
                        <option value="Parbat">Parbat</option>
                        <option value="Parsa">Parsa</option>
                        <option value="Pyuthan">Pyuthan</option>
                        <option value="Ramechhap">Ramechhap</option>
                        <option value="Rasuwa">Rasuwa</option>
                        <option value="Rautahat">Rautahat</option>
                        <option value="Rolpa">Rolpa</option>
                        <option value="Rukum">Rukum</option>
                        <option value="Rupandehi">Rupandehi</option>
                        <option value="Salyan">Salyan</option>
                        <option value="Sankhuwasabha">Sankhuwasabha</option>
                        <option value="Saptari">Saptari</option>
                        <option value="Sarlahi">Sarlahi</option>
                        <option value="Sindhuli">Sindhuli</option>
                        <option value="Sindhupalchok">Sindhupalchok</option>
                        <option value="Siraha">Siraha</option>
                        <option value="Solukhumbu">Solukhumbu</option>
                        <option value="Sunsari">Sunsari</option>
                        <option value="Surkhet">Surkhet</option>
                        <option value="Syangja">Syangja</option>
                        <option value="Tanahu">Tanahu</option>
                        <option value="Taplejung">Taplejung</option>
                        <option value="Terhathum">Terhathum</option>
                        <option value="Udayapur">Udayapur</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>AGE GROUP </label><br>
                      <b> 10</b> <input name="age" style="width:90%;" id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="80" data-slider-step="1" data-slider-value="[15,30]"/> <b>80</b>
                    </div>
                    
                    <div class="form-group">
                      <label>INTEREST :</label> <br>
                      <select name="interest[]" class="selectpicker" multiple data-live-search="true" required>
                        <?php 
                          $interestSql = "SELECT * FROM interests";
                          $interestResult = mysqli_query($conn, $interestSql);
                                    
                            while ($row = mysqli_fetch_assoc($interestResult)) {
                                echo '<option value="'.$row['title'].'">'.$row['title'].'</option>';
                            }
                        ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>How many cutomers do you wanna reach out to?? </label> <br>
                      <div class="slidecontainer">
                      <input name="viewers" step="0.5" type="range" min="0.5" max="100" value="" class="slider1" id="myRange1">
                      <p style="font-size:25px;">Viewers: <b><span id="demo1"></span>K</b></p>
                    </div>
                    </div>

                    <div class="form-group">
                      <label>Amount you want to spend Per Viewer?</label> <br>
                      <div class="slidecontainer">
                      <input name="reach_out" step="1" type="range" min="1" max="50" value="" class="slider" id="myRange">
                      <p style="font-size:25px;">Cost Per Viewer: Nrs.<b><span id="demo"></span></b></p>
                    </div>
                    </div>
                    <p style="font-size:25px;">Total Cost: Nrs.<b><span id="demo2"></span></b></p>
                    Your Current Balance: Nrs. <b><span id="demo5"><?php echo $money;?></span></b>
                    
                    <div class="form-group text-right">
                        <button type="Submit" name="submit" id="submit_btn" class="btn btn-rounded btn-primary" disabled>Submit</button>
                    </div>
                </form>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- /.content -->

    <script type="text/javascript">
      // With JQuery
        // $("#ex2").slider({});

        // Without JQuery
        var slider = new Slider('#ex2', {});
    </script>
    <script>
      var slider1 = document.getElementById("myRange1");
      var output1 = document.getElementById("demo1");
      var slider3 = document.getElementById("myRange");
      var output2 = document.getElementById("demo2");
      var mon = parseInt(document.getElementById("demo5").innerHTML, 10);
      output1.innerHTML = slider1.value;

      slider1.oninput = function() {
        output1.innerHTML = this.value;
        output2.innerHTML = this.value * slider3.value*1000;
        if (parseInt(output2.innerHTML,10) > mon) {
          document.getElementById("submit_btn").disabled = true;
          document.getElementById("demo5").style.color = "red";
        }else{
          document.getElementById("submit_btn").disabled = false;
          document.getElementById("demo5").style.color = "black";
        }
      }
      </script>

    <script>
      var slider = document.getElementById("myRange");
      var output = document.getElementById("demo");
      var output2 = document.getElementById("demo2");
      var slider2 = document.getElementById("myRange1");
      var mon = parseInt(document.getElementById("demo5").innerHTML, 10);
      output.innerHTML = slider.value;
      
      slider.oninput = function() {
        output.innerHTML = this.value;
        output2.innerHTML = this.value * slider1.value*1000;
        
        if (parseInt(output2.innerHTML,10) > mon) {
          document.getElementById("submit_btn").disabled = true;
          document.getElementById("demo5").style.color = "red";
        }else{
          document.getElementById("submit_btn").disabled = false;
          document.getElementById("demo5").style.color = "black";
        }
      }

      </script>
      <script type="text/javascript">
      function _(el) {
  return document.getElementById(el);
}

function uploadFile() {
  var file = _("file1").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file1", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.open("POST", "../pages/target audio/loadaudio.php"); // http://www.developphp.com/video/JavaScript/File-Upload-Progress-Bar-Meter-Tutorial-Ajax-PHP
  //use file_upload_parser.php from above url
  ajax.send(formdata);
}

function progressHandler(event) {
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent) + "% uploaded";
}

function completeHandler(event) {
  // _("status").innerHTML = event.target.responseText;
  // _("progressBar").value = 0; //wil clear progress bar after successful upload
}

function errorHandler(event) {
  _("status").innerHTML = "Upload Failed";
}

function abortHandler(event) {
  _("status").innerHTML = "Upload Aborted";
}
    </script>
  



 