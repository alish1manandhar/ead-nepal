<?php
use MediaFile;

try {
  $media = MediaFile::open('sample.mp3');
  // for audio
  if ($media->isAudio()) {
    $audio = $media->getAudio();
    echo 'Duration: '.$audio->getLength().PHP_EOL;
    echo 'Bit rate: '.$audio->getBitRate().PHP_EOL;
    echo 'Sample rate: '.$audio->getSampleRate().PHP_EOL;
    echo 'Channels: '.$audio->getChannels().PHP_EOL;
  }
  // for video
  else {
    $video = $media->getVideo();
    // calls to VideoAdapter interface
    echo 'Duration: '.$video->getLength().PHP_EOL;
    echo 'Dimensions: '.$video->getWidth().'x'.$video->getHeight().PHP_EOL;
    echo 'Framerate: '.$video->getFramerate().PHP_EOL;
  }
} catch (wapmorgan\MediaFile\Exceptions\FileAccessException $e) {
  // FileAccessException throws when file is not a detected media
} catch (wapmorgan\MediaFile\Exceptions\ParsingException $e) {
   echo 'File is propably corrupted: '.$e->getMessage().PHP_EOL;
}
?>