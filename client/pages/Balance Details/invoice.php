<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Eads Nepal</title>
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }
        
        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }
        
        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }
        
        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }
        
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }
        
        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }
        
        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }
        
        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }
        
        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }
        
        .invoice-box table tr.item.last td {
            border-bottom: none;
        }
        
        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }
        
        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }
            
            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
        
        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }
        
        .rtl table {
            text-align: right;
        }
        
        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
    <?php
    include '../../session.php';
    include '../../connection.php';
    $name = "";
    $company = "";

    $sqli1 = "SELECT * FROM client where id = $aid ";
    $qryi1 = mysqli_query($conn,$sqli1);

    while($r = mysqli_fetch_array($qryi1)){
        $name = $r['f_name']." ".$r['l_name'];
        $company= $r['company_name'];
    }

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $sqli = "SELECT * FROM balance_transfer_history where fk_client_id = $aid and id = $id";
        $qryi = mysqli_query($conn,$sqli);
        while($row = mysqli_fetch_array($qryi)){
            $id = $row['id'];
            $transfered_date= $row['transfered_date'];
            $amount= $row['balance'];
        }
        
        
    }
    ?>
    <div  class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="https://eadnepal.com/home/images/first_logo.png" style="width:100%; max-width:300px;">
                            </td>
                            <td>
                                Invoice #: <?php echo $id; ?><br>
                                Created: <?php echo $transfered_date; ?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Eads Nepal Pvt. Ltd<br>
                                Jhansikhel Chowk<br>
                                Kathmandu, Nepal
                            </td>
                            
                            <td>
                                <?php echo $name; ?><br>
                                <?php echo $company; ?><br>
                                <?php echo $aemail; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Balance Tansfered
                </td>
                
                <td>
                    Amount
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Transefered Amount
                </td>
                
                <td>
                    <?php echo $amount;?>
                </td>
            </tr>
        </table>
        
    </div>

</body>

</html>