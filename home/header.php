<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>e-ad nepal</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600|Roboto:400,400i,500" rel="stylesheet">
	<link rel="stylesheet" href="./css/linearicons.css">
	<link rel="stylesheet" href="./css/font-awesome.min.css">
	<link rel="stylesheet" href="./css/bootstrap.css">
	<link rel="stylesheet" href="./css/owl.carousel.css">
	<link rel="stylesheet" href="./css/main.css">
	<link rel="shortcut icon" href="./images/header_icon.png" type="image/x-icon"/>
</head>

<body>
	<!-- start header Area -->
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="index.php"><img class="header_logo" alt="logo"  style="height: 70px; "/></a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li class="menu-active"><a href="index.php">HOME</a></li>
						<li><a href="aboutus.php">ABOUT</a></li>
						<li><a href="contactus.php">CONTACT</a></li>
						<li class="menu-has-children"><a href="">SIGN UP</a>
							<ul>
								<li><a href="client_signup.php">CLIENT</a></li>
								<li><a href="user_signup.php">USER</a></li>
							</ul>
						</li>
						<li class="menu-has-children"><a href="">LOGIN</a>
							<ul>
								<li><a href="client_login.php">CLIENT</a></li>
								<li><a href="user_login.php">USER</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- end header Area -->