<?php
if ($searchResult->num_rows > 0) {
        while($rowPhoto = $searchResult->fetch_assoc()) {
        ?>
            <div id="<?php echo $rowPhoto['id']; ?>" class="post-id col-md-4"  style="margin-bottom: 20px;">
              <div class="thumbnail"  style="min-height: 350px;">
                <img src="./../../client/pages/target/uploads/<?php echo $rowPhoto['doc_image'];?> " alt="Slide34" style="width: 100%; height: 180px; object-fit: cover;">
                <div class="caption">
                  <div style="min-height: 50px;">
                  <h5><?php echo $rowPhoto['a_title']; ?></h5>
                </div>
                  <?php $user = (80/100)*$rowPhoto['reach_out_price']; ?>
                  <p>Cost: <?php echo $user; ?></p>
                  <a  href="#" onclick="timerz(<?php echo $rowPhoto['id']; ?>)" class="add-to-cart" role="button" data-toggle="modal" data-target="#VideoAdsModel<?php echo $rowPhoto['id']; ?>" >VIEW ADS</a>
                </div>
              </div> 
          </div>

          <!-- Modal -->
        <div class="modal fade" data-keyboard="false" data-backdrop="static"   id="VideoAdsModel<?php echo $rowPhoto['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <a id="replay<?php echo $rowPhoto['id']; ?>" href="#" onclick="timerz(<?php echo $rowPhoto['id']; ?>)" class="add-to-cart" hidden>Replay</a>
                <div id ="timerx" align="center" class="timer<?php echo $rowPhoto['id']; ?>">Time Remaining: 15s</div>
                <img src="../../client/pages/target/uploads/<?php echo $rowPhoto['doc_image']; ?>" alt="Slide34" style="width:100%;">
                <div style="margin-top: 25px;">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12"  style="text-align: justify;">
                  <h5><?php echo $rowPhoto['a_title']; ?></h5>
                  <p style="color: black;"><?php echo $rowPhoto['des']; ?></p>
                </div>
                <hr>
                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                  <i class="fa fa-phone-square" style="color: black; font-size: 18px;"> <?php if ($rowPhoto['phone'] == NULL) {
                    echo "No Contact";
                    }else{
                      echo $rowPhoto['phone'];
                    } ?></i>
                </div>

                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left" >
                  <i class="fa fa-envelope" style="color: black; font-size: 18px;"> <?php if ($rowPhoto['email'] == NULL) {
                    echo "No Email";
                    }else{
                      echo "<a href='mailto:".$rowPhoto['email']."' target='_blank'>Send Mail</a>";
                    } ?></i>

                </div>

                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                  <i class="fa  fa-globe" style="color: black; font-size: 18px;"> <?php if ($rowPhoto['url'] == NULL) {
                    echo "No Website";
                    }else{
                      echo "<a href='https://".$rowPhoto['url']."' target='_blank'>Visit Website</a>";
                    } ?></i>
                </div>
              </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="close_button" onClick="location.reload();" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
          
        <?php
        }
    }else{
        echo '
        <script>
        swal("Sorry, No More Ads!!");
$("#loadmore2").addClass("intro");
</script>';
    }

    ?>