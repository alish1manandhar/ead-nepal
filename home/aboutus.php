<?php
include_once'header.php';
?>
<style type="text/css">
	html,body{
		color: white;
	}
	h1,h2,h3,h4,h5{
		color: white;
	}

	

	.about-title{
		margin-bottom: 20px;
	}
	.toogle_inner p{
		text-align: justify;
	}
   

.toggle-title {
position: relative;
display: block;
border-bottom:  1px solid lightgrey;
margin-bottom: 6px;
   }
.toogle_inner {
padding: 7px 25px 10px 25px;
display: block;
margin: -7px 0 6px;
}

.toogle-faq{
	padding-bottom: 20px;
}

.about-page{
	margin-top: 50px;
}
.toogle_inner p{
    color:white;
}


</style>
<section class="register  section-gap-home">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 home-about-left">
					<img class="img-fluid" src="./../images/mobile.png" alt="">
				</div>
				<div class="col-lg-7 home-about-right">
					<h1>
						ABOUT US<br>
					</h1>
						We are more than just a marketing agency.<br>
						We are innovative and custructive brand marketing and public relation solutions which help our clients grow their business and realize their marketing goals.
						<br><br>
						<h4>Our Services</h4><br>
						Clients:
						<ul style="list-style-type:disc; margin-left:18px">
							<li>Get direct in touch with potential customer or targeted beneficiary</li>
							<li>View reports of advertisement added</li>
							<li>View detailed graph and analytics</li>
							<li>filter reports as required and many more...</li>
						</ul><br>
						<h4>Users:</h4>
						<ul style="list-style-type:disc; margin-left:18px">
							<li>Earn money, recharge phone bills, get gift vouchers by viewing advertisement</li>
							<li>Connect with people, groups, businesses, organizations and institutions</li>
							<li>Can control what they want to watch and many more</li>
						</ul><br>
						<h4>Digital</h4><br>
						Everything that comes from the digital department is custom designed and developed – they consistently deliver smart, innovative websites and digital solutions to help you stand out
						<ul style="list-style-type:disc; margin-left:18px">
							<li>Website</li>
							<li>App</li>
						</ul><br>
				</div>
			</div>
		</div>

<!-- Start Testimonials Area -->
	<section class="testimonials-area about-page section-gap-bottom">
		<div class="container">
			<div about-title align="center">
			<div align="center">
			<h1>ABOUT THE TEAM</h1>
			</div>
			<div class="testi-slider owl-carousel" data-slider-id="1">
				<div class="item">
					<div class="testi-item">
						<img src="./images/quote.png" alt="">
						<h4>Aakash Karkee</h4>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								Web Department Head. <br>
								Email: aakash@eadnepal.com
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="./images/quote.png" alt="">
						<h4>Avinash Mishra</h4>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								Mobile Department Head & Analyst.<br>
								Email: avinash@eadnepal.com
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="./images/quote.png" alt="">
						<h4>Alish Manandhar</h4>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								Managing Director<br>
								Email: alish@eadnepal.com
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="./images/quote.png" alt="">
						<h4>Azay Khadka</h4>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								Marketing Director<br>
								Email: azay@eadnepal.com
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="owl-thumbs d-flex justify-content-center" data-slider-id="1">
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="./images/testimonial/t1.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="./images/testimonial/t2.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="./images/testimonial/t3.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="./images/testimonial/t4.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="toogle-faq">
	<div class="container">
		<div class="about-title" align="center">
		<h2>FAQ's</h2>
		</div>
	<div class="toggle">
	<div class="toggle-title">

		<h4 class="title-name">WHAT IS E-AD ?</h4>
		
	</div>
	<div class="toogle_inner">
		<p>We are more than just a marketing agency.
		We are innovative and custructive brand marketing and public relation solutions which help our clients grow their business and realize their marketing goals. </p>
	</div>
	</div><!-- END OF TOGGLE -->
	<div class="about-title" align="center">
		<h2>USER</h2>
		</div>
		
	<div class="toggle">
		<div class="toggle-title">
		
			<h4 class="title-name">HOW CAN I SIGNUP FOR USER?</h4>
			
		</div>
		
		<div class="toogle_inner">
			<p>It’s simple and easy. Just fill the Sign Up form [sign up form ] with your details. Once you Sign Up you will receive a password on your registered email. When you LOG IN for the first time, it will ask you to change the password.</p>
		</div>
		
		</div><!-- END OF TOGGLE -->
			
  
  
		<div class="toggle">
			<div class="toggle-title">
				
				<h4 class="title-name">HOW CAN I EARN MONEY?</h4>
				
			</div>
			
			<div class="toogle_inner">
				<p>After registration process, you can log in to your account and start viewing ads based on your interest. You can not leave the browser or skip the ad. Once its done, the credit will be transferred to your account.</p>
			</div>
			
			</div><!-- END OF TOGGLE -->
			
			
  
  		<div class="toggle">
			<div class="toggle-title">
				
				<h4 class="title-name">DO I HAVE RIGHT TO CHOOSE THE AD?</h4>
				
			</div>
			
			<div class="toogle_inner">
				<p>We always want our customer to engage with useful ads. We have made it easier for user to pick their interest, and we will show ads based on that. We never show ads beside the interest you picked</p>
			</div>
			
			</div><!-- END OF TOGGLE -->


		<div class="toggle">
			<div class="toggle-title">
				
				<h4 class="title-name">HOW CAN I USE MY ACCOUNT CREDITS?</h4>
				
			</div>
			
			<div class="toogle_inner">
				<p>You can redeem your credit through the website or the mobile app. For now, we offer e-sewa transfer, bank transfer and recharge cards. More options will be available in the future.</p>
			</div>
			
			</div><!-- END OF TOGGLE -->


		<div class="about-title" align="center">
		<h2>CLIENT</h2>
		</div>
		
	<div class="toggle">
		<div class="toggle-title">
		
			<h4 class="title-name">HOW CAN I SIGNUP FOR CLIENT?</h4>
			
		</div>
		
		<div class="toogle_inner">
			<p>You can fill our client form through the website. After filling your comapny’s details and pan/registration number , you have to upload your company’s pan/registration copy as well. Once your processed application successfully verify by our team, you will receive an email to activate your account.</p>
		</div>
		
		</div><!-- END OF TOGGLE -->
			
  
  
		<div class="toggle">
			<div class="toggle-title">
				
				<h4 class="title-name">BENEFITS OF E-AD FOR CLIENT?</h4>
				
			</div>
			
			<div class="toogle_inner">
				<p>* Get direct in touch with potential customer or targeted beneficiary.<br>
				   * View reports of advertisement added.<br>
				   * View detailed graph and analytics.<br>
                   * filter reports as required and many more...</p>
			</div>
			
			</div><!-- END OF TOGGLE -->
  
  
		</div>
	</section>
	</section>


<?php
	include_once'footer.php';
?>