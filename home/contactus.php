<?php
include_once'header.php';
include_once'../connection.php';
if (isset($_POST['submit'])) {
	$name = mysqli_real_escape_string($conn,ucfirst($_POST['name']));
	$subject = mysqli_real_escape_string($conn,ucfirst($_POST['subject']));
	$email = mysqli_real_escape_string($conn,ucfirst($_POST['email']));
	$contact = mysqli_real_escape_string($conn,ucfirst($_POST['contact']));

	 mail("ead.nepal@gmail.com","Contact Us Form","Name: ".$name." Subject:".$subject." Email:".$email." Contact:".$contact."");
}
?>
<style type="text/css">

	  @media (max-width: 575.98px) {

 .maps iframe{
    min-width: 100%;
    height: 250px;
    margin: 0;
  }

 }


@media (min-width: 576px) {
}

   .maps iframe{
     min-width: 100%;
     height: 250px;
    margin: 0;
   }
 }

@media (min-width: 768px) {
}

  .maps iframe{
    min-width: 100%;
    height: 370px;
    margin: 0;
  }
}

@media (min-width: 992px) {}

   .maps iframe{
     min-width: 100%;
     height:420px;
    margin: 0;
    padding: 
   }
}
</style>
	<!-- Start fact Area -->
	<section class="register section-gap-home">
		<div class="text-center" style="margin-bottom: 30px;">
				<h2 style="color: white;">GET IN TOUCH!</h2>
		</div>
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 maps">
			       <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=jhamsikhel&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>
			            
			</div>
		</div>
		<div class="container">
			
			<div class="row" align="center">
			      <form action="" method="post">
			        <label>
				    <p class="label-txt">NAME</p><br>
				    <input type="text" name="name" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  	</label>


				  	<label>
				    <p class="label-txt">SUBJECT</p><br>
				    <input type="text" name="subject" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  	</label>


				  	<label>
				    <p class="label-txt">EMAIL</p><br>
				    <input type="email" name="email" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  	</label>

				  	<label>
				    <p class="label-txt">CONTACT</p><br>
				    <input type="number" name="contact" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  	</label>

				  	<div align="right"> 
				  	<button type="submit" name="submit" >SUBMIT</button>
				  </div>
			        </form>
			</div>
			
		</div>
		</div>
		
		
	</section>
	<!-- End fact Area -->


<?php
	include_once'footer.php';
?>