<?php
include_once'header.php';
session_start();

$_SESSION["uemail"] = "";
  $_SESSION["loginStatus"] = 0;
  $_SESSION["uid"] = 0;
  $errr = "";
?>
	<!-- Start fact Area -->
	<section class="register section-gap-home">
		<div class="container">
			<div class="text-center" >
				<h2 style="color: white;">CLIENT LOGIN</h2>
				<p style="color: white; margin-top: 10px;">
					<?php echo $errr;?>
			      <?php
			        if (isset($_SESSION['forgotError'])) {
			            echo $_SESSION['forgotError'];
			                unset($_SESSION['forgotError']);
			          }
			    if (isset($_SESSION['uploadErr'])) {
			        echo $_SESSION['uploadErr'];
			        unset($_SESSION['uploadErr']);
			    } 

			    if (isset($_SESSION['loginErr'])) {
			    		echo $_SESSION['loginErr'];
			    		unset($_SESSION['loginErr']);	
			    	}
				  ?>
				</p>
			</div>
			<div class="row">
				<form action="../client/login/" method="post" enctype="multipart/form-data">
				  <label>
				    <p class="label-txt">EMAIL</p><br>
				    <input type="email" name="user" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  <label>
				    <p class="label-txt">PASSWORD</p><br>
				    <input type="password" name="pass" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  	<label>
				  		<a href="./client_forgotpass.php">Forgot Password?</a>  
				  	</label>

				  <div align="right"> 
				  	<button type="submit" name="submit" >LOGIN</button>
				  </div>
				  
				</form>
				
			</div>
		</div>
		
		
	</section>
	<!-- End fact Area -->


<?php
	include_once'footer.php';
?>