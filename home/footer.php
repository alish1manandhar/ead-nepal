	<!-- Start Footer Area -->
	<footer class="footer-area section-gap-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
					<img src="./images/icon_white.svg" style="width: 100px; height: 100px;"><br><br>
					<ul>
						<li>Got Questions?</li>
						<li>Call Us: 01-5548938</li>
						<li>Jhamshikhel, Lalitpur</li>
					</ul>
					<div class="footer_social">
							<ul>
								<li><a target="_blank" href="https://www.facebook.com/Ead-Nepal-298048397758920/"><i class="fa fa-facebook-f"></i></a></li>
								<li><a target="_blank" href="https://instagram.com/ead.nepal?igshid=13wywla2jd6h3"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="user_login.php">Picture Ads</a></li>
						<li><a href="user_login.php">Video Ads</a></li>
						<li><a href="user_login.php">Audio Ads</a></li>
					</ul>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
					<div class="download-button flex-row ">
						<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true" ></i>
							<div class="desc">
									<p>
										<span>Soon</span> <br>
										on App Store
									</p>
							</div>
						</div><br>
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a target="_blank" href="https://play.google.com/store/apps/details?id=com.azwraithnp.eadnepal">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>

					
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
					<h4>Newsletter</h4>
					<p>You can trust us. we only send promo offers,</p>
					<div class="form-wrap" id="mc_embed_signup">
						<form target="_blank" action=""
						 method="get" class="form-inline">
							<input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
							 required="" type="email">
							<button class="click-btn btn btn-default"><span class="lnr lnr-arrow-right"></span></button>
							
						</form>
					</div>
				</div>
			</div>
			<div class="footer-bottom row align-items-center">
				<p class="footer-text m-0 col-lg-12 col-md-12 col-sm-12 col-xs-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				
			</div>
		</div>
	</footer>
	<script src="./js/vendor/jquery-2.2.4.min.js"></script>
	<script src="./js/tilt.jquery.min.js"></script>
	<script src="./js/vendor/bootstrap.min.js"></script>
	<script src="./js/superfish.min.js"></script>
	<script src="./js/jquery.magnific-popup.min.js"></script>
	<script src="./js/owl.carousel.min.js"></script>
	<script src="./js/owl-carousel-thumb.min.js"></script>
	<script src="./js/jquery.nice-select.min.js"></script>
	<script src="./js/waypoints.min.js"></script>
	<script src="./js/main.js"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script type="text/javascript">
		$( document ).ready(function() {
		    $('.header_logo').attr("src","./images/banner.png");
		});

		 if( jQuery(".toggle .toggle-title").hasClass('active') ){
        jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toogle_inner').show();
    }
    jQuery(".toggle .toggle-title").click(function(){
        if( jQuery(this).hasClass('active') ){
            jQuery(this).removeClass("active").closest('.toggle').find('.toogle_inner').slideUp(200);
        }
        else{   jQuery(this).addClass("active").closest('.toggle').find('.toogle_inner').slideDown(200);
        }
    });
	</script>
</body>

</html>