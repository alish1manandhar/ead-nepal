<?php 
	include_once '../user/header.php';
	$id = mysqli_real_escape_string($conn,ucfirst($_GET['variable']));

	$videoSql = "SELECT * FROM add_video_ads WHERE id = '".$id."'"; 
	$videoResult = $conn->query($videoSql);
	if ($videoResult->num_rows > 0) {
        $rowVideo = $videoResult->fetch_assoc();
  }
?>
<style type="text/css">
    .caption .add-to-cart {
        display: inline-block;
        width: auto;
        border: 2px solid #20638f;
        padding: 0.4em 0.6em;
        color: #20638f;
        text-decoration: none;
        font-weight: 800;
        font-size: 0.9em;
        text-transform: uppercase;
      }
      .caption .add-to-cart:hover {
        background-color: #20638f;
        color: #ffffff;
      }

      .caption h3{
        min-height: 50px;
      }
</style>
    <section class="register section-gap-top">
      <div class="container" id="ad_category" >
        <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../video/" style=" color: white;">Video</a> / <a href="#" style=" color: white;">Video Detail</a>
        </div><br>
        <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
          <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3" id="category_list">
            <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">CATEGORIES
              </a>
                <a href="./../picture/" class="list-group-item list-group-item-action">PICTURE </a>
                <a href="./../video/" class="list-group-item list-group-item-action">VIDEO </a>
                <a href="./../audio/" class="list-group-item list-group-item-action">AUDIO </a>
            </div>
          </div>

          <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9" style="background-color: rgba(245, 245, 245, 0.7); padding: 25px 25px 25px 25px;">
              <video id='my-video' class='video-js' onended="setVideoViewers(<?php echo $rowVideo['id']; ?>);" controls style="height: 400px; width: 100%;" data-setup='{}' autoplay="true">
                <source src="../../client/pages/target video/uploads/<?php echo $rowVideo['video']; ?>" type='video/mp4'>
                <source src="../../client/pages/target video/uploads/<?php echo $rowVideo['video']; ?>" type='video/mov'>
                <source src="../../client/pages/target video/uploads/<?php echo $rowVideo['video']; ?>" type='video/flv'>
              </video>
              <div style="margin-top: 25px;">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12"  style="text-align: justify;">
                  <h5><?php echo $rowVideo['a_title']; ?></h5>
                  <p style="color: black;"><?php echo $rowVideo['des']; ?></p>
                </div>
                <hr>
                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                  <i class="fa fa-phone-square" style="color: black; font-size: 18px;"> <?php if ($rowVideo['phone'] == NULL) {
                    echo "No Contact";
                    }else{
                      echo $rowVideo['phone'];
                    } ?></i>
                </div>

                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left" >
                  <i class="fa fa-envelope" style="color: black; font-size: 18px;"> <?php if ($rowVideo['email'] == NULL) {
                    echo "No Email";
                    }else{
                      echo "<a href='mailto:".$rowVideo['email']."' target='_blank'>Send Mail</a>";
                    } ?></i>

                </div>

                <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                  <i class="fa  fa-globe" style="color: black; font-size: 18px;"> <?php if ($rowVideo['url'] == NULL) {
                    echo "No Website";
                    }else{
                      echo "<a href='https://".$rowVideo['url']."' target='_blank'>Visit Website</a>";
                    } ?></i>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="text-center">
            <h3 style="color: white;">RELATED VIDEO ADS</h3>
          </div>
            <div class="well">
                <!-- Carousel
                ================================================== -->            
                <div class="carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                             <?php
                             $relatedSql = "SELECT * from add_video_ads WHERE (target_gender ='all' OR  target_gender='".$sex."') AND (location LIKE 'all%' OR location LIKE '%".$location."%') AND initial_price >= 1 AND min_age <='".$age."' AND max_age >='".$age."' AND id != '".$rowVideo['id']."' AND id NOT IN  (SELECT fk_ads from videoviews WHERE fk_viewer = $uid) ORDER BY rand() DESC LIMIT 4";
                            $relatedResult = $conn->query($relatedSql);
                            if ($relatedResult->num_rows > 0):
                            while($rowrelated = $relatedResult->fetch_assoc()):
                            ?>
                            <div id="<?php echo $rowrelated['id']; ?>" class="post-id col-md-3">
                              <div class="thumbnail" style="min-height: 350px;">
                               <video width="100%" nocontrol>
                                <source src="../../client/pages/target video/uploads/<?php echo $rowrelated['video']; ?>" type="video/mp4">
                                <source src="../../client/pages/target video/uploads/<?php echo $rowrelated['video']; ?>" type="video/mov">
                                <source src="../../client/pages/target video/uploads/<?php echo $rowrelated['video']; ?>" type="video/flv">
                              </video> 
                                <div class="caption">
                                  <div style="min-height: 50px;">
                                    <h5><?php echo $rowrelated['a_title']; ?></h5>
                                  </div>
                                  <?php $user = (70/100)*$rowrelated['reach_out_price']; ?>
                                      <p>Cost: <?php echo $user; ?></p>

                                  <?php
                                  $randPass = rand(1111111111,999999999);
                                  $realPass = md5($randPass);
                                  ?>
                                  <a  href="videoDetail.php?variable=<?php echo $rowrelated['id']; ?>&value=<?php echo $realPass; ?>" class="add-to-cart"  role="button" >VIEW ADS</a>
                                </div>
                              </div> 
                              </div>
                            <?php endwhile; endif;?>
                            </div>
                        </div>        
                    </div><!-- End Carousel --> 
                </div>
            </div><!-- End Well -->
        </div>
      </div>
    </section>


        <script type="text/javascript">
            document.addEventListener("visibilitychange", onchange);
            function onchange (evt) {
               var myPlayers = videojs("#my-video");
               myPlayers.pause();
            }
        </script>

        <script src="./../js/video.js"></script>
         <script type="text/javascript">
          var myPlayer = videojs('#my-video', {
            autoplay: true,
            muted: false,
            loop: false,
            controlBar: {
              volumeBar: false,
                bigPlayButton: false,
                playToggle: true,
                timeDivider: true,
                currentTimeDisplay: true,
                durationDisplay: true,
                remainingTimeDisplay: true,
                progressControl: false,
                fullscreenToggle: true,
                volumeMenuButton: false,
                resetButton: false,
            }
        });

        </script>
        <script type="text/javascript">
                function setVideoViewers(a){
                      if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp=new XMLHttpRequest();
                      } else { // code for IE6, IE5
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                      }
                      xmlhttp.onreadystatechange=function() {
                      if (this.readyState==4 && this.status==200) {
                        swal(this.responseText);
                      }
                    }
                    xmlhttp.open("GET","./../user/setVideoViewers.php?id=" + a,true);
                    xmlhttp.send();
                  }
        </script>

<?php 
 	include_once '../user/footer.php';
 ?>