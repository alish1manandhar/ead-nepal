	<?php
	include_once'header.php';
	?>
	<!-- start banner Area -->
	<section class="home-banner-area">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="home-banner-content col-lg-6 col-md-6">
					<h1>
						Digitalizing Relationship<br> In Nepal
					</h1>
					<div class="download-button d-flex flex-row justify-content-start">
						<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true"></i>
							<div class="desc">
									<p>
										<span>Soon</span> <br>
										on App Store
									</p>
							</div>
						</div>
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a target="_blank" href="https://play.google.com/store/apps/details?id=com.azwraithnp.eadnepal">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="banner-img col-lg-4 col-md-6">
					<img class="img-fluid" src="./images/mobile.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start fact Area -->
	<!--
	<section class="fact-area">
		
	</section>
	-->
	<!-- End fact Area -->

	<!-- Start feature Area -->
	<section class="feature-area section-gap-home">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Earn Money In <b>3</b> Easy Steps</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
							<span class="lnr lnr-lock"></span>
							<h3>Sign Up</h3>
						<p class="front-content"> 
							Signing up won’t take a long. Get registered in a minutes and start exploring.
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
							<span class="lnr lnr-eye"></span>
							<h3>View Ads</h3>
						<p class="front-content">
							You have full control to choose what you see. Pick Interest of your ad and boooom.
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
							<span class="lnr lnr-smile"></span>
							<h3>Earn Money</h3>
						<p class="front-content">
							Earning has never been this easy. Watch video, image and audio ads of your selection and start earning.
						</p>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End feature Area -->

	

	<!-- Start Screenshot Area -->
	<section class="screenshot-area section-gap-home">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Featured Screens</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="owl-carousel owl-screenshot">
					<div class="single-screenshot">
						<img class="img-fluid" src="./images/screenshots/s1.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="./images/screenshots/s2.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="./images/screenshots/s3.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="./images/screenshots/s4.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Screenshot Area -->
	<?php
	include '../connection.php';
	$viewers = 0;
	$clients = 0;
	$audios = 0;
	$videos = 0;
	$photos = 0;
	$viewer = "SELECT COUNT(id) AS 'totalviewers' FROM viewers";
        $viewerResult = mysqli_query($conn, $viewer);
        if ($viewerResult) {
        	while($row = mysqli_fetch_assoc($viewerResult)) {
                 $viewers = $row['totalviewers'];   
            }
        }

    $client = "SELECT COUNT(id) AS 'totalclient' FROM client";
        $clientResult = mysqli_query($conn, $client);
        if ($clientResult) {
        	while($row = mysqli_fetch_assoc($clientResult)) {
                 $clients = $row['totalclient'];   
            }
        }


    $photos = "SELECT COUNT(id) AS 'totalphotos' FROM add_ads";
        $photosResult = mysqli_query($conn, $photos);
        if ($photosResult) {
        	while($row = mysqli_fetch_assoc($photosResult)) {
                 $photos = $row['totalphotos'];   
            }
        }


    $audios = "SELECT COUNT(id) AS 'totalaudios' FROM add_audio_ads";
        $audiosResult = mysqli_query($conn, $audios);
        if ($audiosResult) {
        	while($row = mysqli_fetch_assoc($audiosResult)) {
                 $audios = $row['totalaudios'];   
            }
        }


    $videos = "SELECT COUNT(id) AS 'totalvideos' FROM add_video_ads";
        $videosResult = mysqli_query($conn, $videos);
        if ($videosResult) {
        	while($row = mysqli_fetch_assoc($videosResult)) {
                 $videos = $row['totalvideos'];   
            }
        }

        	
        
	?>
	<!-- Start Pricing Area -->
	<section class="pricing-area">
		<div class="container">
			<div class="fact-box">
				<div class="row align-items-center">
					<div class="col single-fact viewers-box">
						<h2><?php echo  $viewers; ?></h2>
						<p>VIEWERS</p>
					</div>
					<div class="col single-fact viewers-box">
						<h2><?php echo  $photos; ?></h2>
						<p>PICTURE ADS</p>
					</div>
					<div class="col single-fact viewers-box">
						<h2><?php echo  $videos; ?></h2>
						<p>VIDEO ADS</p>
					</div>
					<div class="col single-fact viewers-box">
						<h2><?php echo  $audios; ?></h2>
						<p>AUDIO ADS</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Pricing Area -->
	

	


	<?php
	include_once'home_footer.php';
	?>