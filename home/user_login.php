<?php
include_once'header.php';
session_start();

$_SESSION["uemail"] = "";
  $_SESSION["loginStatus"] = 0;
  $_SESSION["uid"] = 0;
  $errr = "";

  if(isset($_POST['submit'])){
    $username = $_POST['email'];
    $password = md5($_POST['password']);
    
    include_once('../connection.php');
    $sql = sprintf("SELECT * FROM viewers WHERE email = '%s' AND password ='%s'",
            mysqli_real_escape_string($conn,$username),
            mysqli_real_escape_string($conn,$password)
      );

    $result=$conn->query($sql);
    $stat = 0;

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()){
                    $id = $row['id'];
                    $_SESSION["uid"] = $id;
                    $_SESSION["uemail"] = $row['email'];
                    $_SESSION['status'] = $row['status'];
                    $stat +=1;
      }
    }

    $count = $result->num_rows;
    if($count==1){
      $_SESSION["loginStatus"] = 1;
      if ($_SESSION['status'] == 0) {
        header("location: ./newuser/interest.php");
      }else if($_SESSION['status'] == 2){
      	header("location: ./newuser/changepassword.php");
      }else{
      header("location: ./user/");
    }
    }else{
          $_SESSION["uemail"] = "";
          $_SESSION["loginStatus"] = 0;
          $_SESSION["uid"] = 0;
      $errr = '<div class="alert alert-danger"><strong>Please enter valid info</strong></div>';
      
    }
  }
?>
	<!-- Start fact Area -->
	<section class="register section-gap-home">
		<div class="container">
			<div class="text-center" >
				<h2 style="color: white;">USER LOGIN</h2>
				<p style="color: white; margin-top: 10px;">
					<?php echo $errr;?>
				      <?php
				        if (isset($_SESSION['forgotError'])) {
				            echo $_SESSION['forgotError'];
				                unset($_SESSION['forgotError']);
				          }
				        ?>
				        <?php
					  if (isset($_SESSION['uploadErr'])) {
					    echo $_SESSION['uploadErr'];
					        unset($_SESSION['uploadErr']);
					  }
					  ?>
				</p>
			</div>
			<div class="row">
				<form action="" method="post" enctype="multipart/form-data">
				  <label>
				    <p class="label-txt">EMAIL</p><br>
				    <input type="email" name="email" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  <label>
				    <p class="label-txt">PASSWORD</p><br>
				    <input type="password" name="password" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  	<label>
				  		<a href="./forgotpass.php">Forgot Password?</a>    
				  	</label>

				  <div align="right"> 
				  	<button type="submit" name="submit" >LOGIN</button>
				  </div>
				  
				</form>
				
			</div>
		</div>
		
		
	</section>


<?php
	include_once'footer.php';
?>