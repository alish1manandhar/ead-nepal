<?php
include_once'header.php';
$userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
$viewerDetail = mysqli_query($conn, $userDetail);
if (mysqli_num_rows($viewerDetail) > 0) {
    // output data of each row
    $rowUser = mysqli_fetch_assoc($viewerDetail);
    $userStatus = $rowUser['status'];
      if ($userStatus != 2){
        header("location: ./interest.php");
      }
}else{
  header('location: ../user_login.php');
}
if (isset($_POST['submit'])) {
      $oldpass = md5(mysqli_real_escape_string($conn,$_POST['oldpass']));
      $newpass = mysqli_real_escape_string($conn,$_POST['newpass']);
      $confirm = mysqli_real_escape_string($conn,$_POST['confirm']);

        if (!($row['password'] == $oldpass)) {
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Old Password Dont Match.</strong></div>';
        }else if(!($newpass == $confirm)){
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>New Password and Confirm Password Dont Match.</strong></div>';
        }elseif (strlen($newpass) < 9) {
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Password Length Must Be Greater Than 9</strong></div>';
        }elseif (!preg_match("#[0-9]+#",$newpass)) {
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain A Number</strong></div>';
        }elseif(!preg_match("#[A-Z]+#",$newpass)) {
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Capital Alphabets</strong></div>';
        }elseif (!preg_match("#[a-z]+#",$newpass)){
          $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Small Alphabets</strong></div>';
        }else{
          $newPassword = md5($newpass);
           $update = "UPDATE viewers SET 
           password = '".$newPassword."',
           status = 1
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              header('Location: ./../user/');
            }else{
              $_SESSION['ChangePasswordError'] = '<div class="alert alert-danger"><strong>Password Update Failed.</strong></div>';
            }
        }
}else{

}
?>
	<!-- Start fact Area -->
	<section class="register section-gap-top">
		<div class="container">
			<div class="text-center" >
				<h2 style="color: white;">CHANGE YOUR PASSWORD FIRST</h2>
        <p style="color: white;">( Password Length Must Be Greater Than 9 And Must Contain A Number, Capital Letter And Small Letter)</p>
				<p style="margin-top: 10px;">
					<?php 
                      if (isset($_SESSION['ChangePasswordError'])) {
                        echo $_SESSION['ChangePasswordError'];
                      }
                    
                    ?>
                    <?php 
                      if (isset($_SESSION['ChangePasswordSuccess'])) {
                        echo $_SESSION['ChangePasswordSuccess'];
                      }
                    
                    ?>
				</p>
				
			</div>
			<div class="row">
				<form action="" method="post" enctype="multipart/form-data">
				  <label>
				    <p class="label-txt">OLD PASSWORD</p><br>
				    <input type="password" name="oldpass" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  <label>
				    <p class="label-txt">NEW PASSWORD</p><br>
				    <input type="password" name="newpass" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  <label>
				    <p class="label-txt">CONFIRM PASSWORD</p><br>
				    <input type="password" name="confirm" class="input" required>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>

				  <div align="right"> 
				  	<div class=" text-right">
              <button type="Submit" name="submit" >Change</button>
          </div>
				  </div>
				  
				</form>
			</div>
		</div>
		
		
	</section>
	<!-- End fact Area -->


<?php
	include_once'footer.php';
?>