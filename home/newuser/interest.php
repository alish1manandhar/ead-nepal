<?php
include_once'header.php';
$userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
$viewerDetail = mysqli_query($conn, $userDetail);

if (mysqli_num_rows($viewerDetail) > 0) {
    $row = mysqli_fetch_assoc($viewerDetail);
    $status = $row['status'];
      if ($status != 0) {
        header("location: ./../user_login.php");
      }
    
}else{
  header('location: ../user_login.php');
}
if (isset($_POST['submit'])) {
	if(!empty($_POST['interest'])){
	$interest = array();
	foreach ($_POST['interest'] as $value) {
		$interest[] = $value;
	}
	$endresult = implode(',' , $interest);
	$update = "UPDATE viewers SET 
       interest = '".$endresult."',
       status = 2
        WHERE id = '".$uid."'";
        $prepareUpdate = mysqli_query($conn, $update);
        if ($prepareUpdate) {
          header('Location: ./changepassword.php');
        }else{
          $_SESSION['interestError'] = '<div class="alert alert-danger"><strong>Interest insertion Failed.</strong></div>';
        }
	}else{
		$_SESSION['interestError'] = '<div class="alert alert-danger"><strong>Please Select One Interest</strong></div>';
	}
}else{

}
?>
	<!-- Start fact Area -->
	<section class="register section-gap-top">
		<div class="container">
			<div class="text-center" >
				<h2 style="color: white;">CHOOSE YOUR INTERESTS FIRST</h2>
				<p style="margin-top: 10px;" style="color: white;">
					<?php
					if (isset($_SESSION['interestError'])) {
                        echo $_SESSION['interestError'];
                        unset($_SESSION['interestError']);
                      }
                      ?>

                      <?php
					if (isset($_SESSION['interestSuccess'])) {
                        echo $_SESSION['interestSuccess'];
                      }
                      ?>
				</p>
				
			</div>
			<div class="row">
				<form method="post" action="" style="min-width: 100%;">
					<?php
					$interestDetail = "SELECT * FROM interests";
					$interestResult = mysqli_query($conn, $interestDetail);
                    while($data = mysqli_fetch_assoc($interestResult)):

					?>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 nopad text-center">
				      <label class="image-checkbox">
				        <img class="img-responsive" src="./../../management/pages/Add Interest/uploads/<?php echo $data['image']; ?>" alt="<?php echo $data['title']; ?>" />
				        <input type="checkbox" name="interest[]" value="<?php echo $data['title']; ?>" />
				        <i class="fa fa-check"></i>
				      </label>
				    </div>
				<?php endwhile; ?>
				    

				    <div align="right" style="margin-right: 1em;">
				    	<div class=" text-right">
		              <button type="Submit" name="submit" >Next</button>
		          </div>
		                
				  </div>
				</form>
			</div>
		</div>
		
		
	</section>
	<!-- End fact Area -->


<?php
	include_once'footer.php';
?>