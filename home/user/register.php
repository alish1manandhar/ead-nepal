  <?php 
    include '../../connection.php'; 
    session_start();
    $_SESSION['uploadErr'] = "";
    $err = 0;
    if(isset($_POST['submit'])){
      if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){ 
        // Google reCAPTCHA API secret key 
            $secretKey = '6Lco0agUAAAAAM6APa8iJ8bN4N5Z4M7VoKbDQeAE'; 
             
            // Verify the reCAPTCHA response 
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['g-recaptcha-response']); 
             
            // Decode json data 
            $responseData = json_decode($verifyResponse); 
            if($responseData->success){ 

      $in = implode(",", $_POST['interest']);
      $f_name = mysqli_real_escape_string($conn,ucfirst($_POST['f_name']));
      $l_name = mysqli_real_escape_string($conn,ucfirst($_POST['l_name']));
      $email = mysqli_real_escape_string($conn,$_POST['email']);
      $phone = mysqli_real_escape_string($conn,$_POST['phone']);
      $dob = mysqli_real_escape_string($conn,$_POST['dob']);
      $location = mysqli_real_escape_string($conn,ucfirst($_POST['location']));
      $college_name = mysqli_real_escape_string($conn,ucfirst($_POST['college']));
      $level = mysqli_real_escape_string($conn,ucfirst($_POST['level']));
      $field_of_study = mysqli_real_escape_string($conn,ucfirst($_POST['field_of_study']));
      $company_name = mysqli_real_escape_string($conn,ucfirst($_POST['c_name']));
      $post = mysqli_real_escape_string($conn,ucfirst($_POST['post']));
      $sex = mysqli_real_escape_string($conn,$_POST['sex']);

      $checkEmail = sprintf("SELECT * FROM viewers WHERE email = '".$email."'",
            mysqli_real_escape_string($conn,$email)
      );
      $emailResult=$conn->query($checkEmail);
      $countEmail = $emailResult->num_rows;
        

      $checkContact = sprintf("SELECT * FROM viewers WHERE phone = '".$phone."'",
            mysqli_real_escape_string($conn,$contact)
      );
      $checkResult=$conn->query($checkContact);
      $countContact = $checkResult->num_rows;


      if ($f_name == "" || $l_name == "" || $phone == "" || $email == "" || $location == "" ) {
        
          $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: General Information Field Was Empty.</strong><p>';
         header('Location: ../user_signup.php');
        $err = 1;
      }else if($level == "" || $field_of_study == ""){
        $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Study Detail Field Was Empty.</strong><p>';
         header('Location: ../user_signup.php');
       }else if ($countEmail == 1) {
         $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Email Already Exist</strong><p>';
         header('Location: ../user_signup.php');
      }else if ($countContact == 1){
          $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Contact Already Exist</strong><p>';
         header('Location: ../user_signup.php');
      }else if(strlen($phone)<>10){
          $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Contact Most Consist Of 10 Numbers.</strong><p>';
         header('Location: ../user_signup.php');

      }else{
       $randPass = rand(1111111111,999999999);
       $realPass = md5($randPass);
        $sql = "INSERT INTO viewers (f_name, l_name, email, phone,location,college,level,field_of_study,company,post,reg_date,password,dob,sex) 
                                  VALUES ('$f_name','$l_name', '$email','$phone','$location','$college_name','$level','$field_of_study','$company_name','$post',CURDATE(),'$realPass','$dob','$sex')";


        $prepareSl = mysqli_query($conn, $sql);

        if($prepareSl) {
                                    //mail("$email","Your Password",$randPass);
                                                        //mail("$email","Your Password",$randPass);
                                  ini_set( 'display_errors', 1 );
              error_reporting( E_ALL );
              $from = "eadsnepal_registration@eadnepal.com";
              $to = $email;
              $subject = "ead nepal : Registration Successful";
              $message = '
                <html>
                <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                  <meta name="viewport" content="width=device-width, initial-scale=1" />
                  <title>Narrative Welcome Email</title>
                  <style type="text/css">

                  /* Take care of image borders and formatting */

                  img {
                    max-width: 600px;
                    outline: none;
                    text-decoration: none;
                    -ms-interpolation-mode: bicubic;
                  }

                  a {
                    border: 0;
                    outline: none;
                  }

                  a img {
                    border: none;
                  }

                  /* General styling */

                  td, h1, h2, h3  {
                    font-family: Helvetica, Arial, sans-serif;
                    font-weight: 400;
                  }

                  td {
                    font-size: 13px;
                    line-height: 19px;
                    text-align: left;
                  }

                  body {
                    -webkit-font-smoothing:antialiased;
                    -webkit-text-size-adjust:none;
                    width: 100%;
                    height: 100%;
                    color: #37302d;
                    background: #ffffff;
                  }

                  table {
                    border-collapse: collapse !important;
                  }


                  h1, h2, h3, h4 {
                    padding: 0;
                    margin: 0;
                    color: #444444;
                    font-weight: 400;
                    line-height: 110%;
                  }

                  h1 {
                    font-size: 35px;
                  }

                  h2 {
                    font-size: 30px;
                  }

                  h3 {
                    font-size: 24px;
                  }

                  h4 {
                    font-size: 18px;
                    font-weight: normal;
                  }

                  .important-font {
                    color: #21BEB4;
                    font-weight: bold;
                  }

                  .hide {
                    display: none !important;
                  }

                  .force-full-width {
                    width: 100% !important;
                  }

                  </style>

                  <style type="text/css" media="screen">
                      @media screen {
                        @import url(http://fonts.googleapis.com/css?family=Open+Sans:400);

                        /* Thanks Outlook 2013! */
                        td, h1, h2, h3 {
                          font-family: "Open Sans", "Helvetica Neue", Arial, sans-serif !important;
                        }
                      }
                  </style>

                  <style type="text/css" media="only screen and (max-width: 600px)">
                    /* Mobile styles */
                    @media only screen and (max-width: 600px) {

                      table[class="w320"] {
                        width: 320px !important;
                      }

                      table[class="w300"] {
                        width: 300px !important;
                      }

                      table[class="w290"] {
                        width: 290px !important;
                      }

                      td[class="w320"] {
                        width: 320px !important;
                      }

                      td[class~="mobile-padding"] {
                        padding-left: 14px !important;
                        padding-right: 14px !important;
                      }

                      td[class*="mobile-padding-left"] {
                        padding-left: 14px !important;
                      }

                      td[class*="mobile-padding-right"] {
                        padding-right: 14px !important;
                      }

                      td[class*="mobile-padding-left-only"] {
                        padding-left: 14px !important;
                        padding-right: 0 !important;
                      }

                      td[class*="mobile-padding-right-only"] {
                        padding-right: 14px !important;
                        padding-left: 0 !important;
                      }

                      td[class*="mobile-block"] {
                        display: block !important;
                        width: 100% !important;
                        text-align: left !important;
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                        padding-bottom: 15px !important;
                      }

                      td[class*="mobile-no-padding-bottom"] {
                        padding-bottom: 0 !important;
                      }

                      td[class~="mobile-center"] {
                        text-align: center !important;
                      }

                      table[class*="mobile-center-block"] {
                        float: none !important;
                        margin: 0 auto !important;
                      }

                      *[class*="mobile-hide"] {
                        display: none !important;
                        width: 0 !important;
                        height: 0 !important;
                        line-height: 0 !important;
                        font-size: 0 !important;
                      }

                      td[class*="mobile-border"] {
                        border: 0 !important;
                      }
                    }
                  </style>
                </head>
                <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
                <table align="center" cellpadding="0" cellspacing="0" width="100%" height="100%">
                  <tr>
                    <td align="center" valign="top" bgcolor="#ffffff"  width="100%">

                    <table cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                        <td style="background:#1f1f1f" width="100%">

                          <center>
                            <table cellspacing="0" cellpadding="0" width="600" class="w320">
                              <tr>
                                <td valign="top" class="mobile-block mobile-no-padding-bottom mobile-center" width="270" style="background:#1f1f1f;padding:10px 10px 10px 20px;">
                                  <a href="#" style="text-decoration:none;">
                                    <img src="https://eadnepal.com/apis/email/banner.png" width="142" height="30" alt="Logo"/>
                                  </a>
                                </td>
                                <td valign="top" class="mobile-block mobile-center" width="270" style="background:#1f1f1f;padding:10px 15px 10px 10px">
                                  <table border="0" cellpadding="0" cellspacing="0" class="mobile-center-block" align="right">
                                    <tr>
                                      <td align="right">
                                        <a href="#">
                                        <img src="http://keenthemes.com/assets/img/emailtemplate/social_facebook.png"  width="30" height="30" alt="social icon"/>
                                        </a>
                                      </td>
                                      
                                      <td align="right" style="padding-left:5px">
                                        <a href="#">
                                        <img src="http://keenthemes.com/assets/img/emailtemplate/social_googleplus.png"  width="30" height="30" alt="social icon"/>
                                        </a>
                                      </td>
                                      
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </center>

                        </td>
                      </tr>
                      <tr>
                        <td style="border-bottom:1px solid #e7e7e7;">

                          <center>
                            <table cellpadding="0" cellspacing="0" width="600" class="w320">
                              <tr>
                                <td align="left" class="mobile-padding" style="padding:20px 20px 0">

                                  <br class="mobile-hide" />

                                  <h1>Welcome to ead nepal</h1>

                                  <br>
                                  We are excited you are here! Your password for the account is '.$randPass.'<br>
                                  <br>

                                  <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff">
                                    <tr>
                                      <td style="width:130px;background:#D84A38;">
                                        <div>
                                          <!--[if mso]>
                                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:33px;v-text-anchor:middle;width:130px;" stroke="f" fillcolor="#D84A38">
                                            <w:anchorlock/>
                                            <center>
                                          <![endif]-->
                                              <a href="https://eadnepal.com/home/user_login.php"
                                        style="background-color:#D84A38;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:33px;text-align:center;text-decoration:none;width:130px;-webkit-text-size-adjust:none;">Activate!</a>
                                          <!--[if mso]>
                                            </center>
                                          </v:rect>
                                          <![endif]-->
                                        </div>
                                      </td>
                                      <td width="316" style="background-color:#ffffff; font-size:0; line-height:0;">&nbsp;</td>
                                    </tr>
                                  </table>
                                  <br><br>
                                </td>
                                <td class="mobile-hide" style="padding-top:20px;padding-bottom:0;vertical-align:bottom">
                                  <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                      <td align="right" valign="bottom" width="220" style="padding-right:20px; padding-bottom:0; vertical-align:bottom;">
                                      <img  style="display:block" src="https://eadnepal.com/apis/email/mobile_sc.png"  width="174" height="294" alt="iphone"/>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </center>

                        </td>
                      </tr>
                   
                      <tr>
                        <td style="background-color:#1f1f1f;">
                          <center>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" class="w320" style="height:100%;color:#ffffff" bgcolor="#1f1f1f" >
                              <tr>
                                <td align="right" valign="middle" class="mobile-padding" style="font-size:12px;padding:20px; background-color:#1f1f1f; color:#ffffff; text-align:left; ">
                                  <a style="color:#ffffff;"  href="#">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a style="color:#ffffff;" href="#">Facebook</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a style="color:#ffffff;" href="#">Support</a>
                                </td>
                              </tr>
                            </table>
                          </center>
                        </td>
                      </tr>
                    </table>

                    </td>
                  </tr>
                </table>
                </body>
                </html>';
              $headers = "From:" . $from;
              $headers .= "\r\nMIME-Version: 1.0\r\n";
              $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
              mail($to,$subject,$message, $headers);
              echo "The email message was sent.";
                                    $_SESSION['uploadErr'] = '<p class="alert alert-success"><strong>Success: Signed Up Successfully. Check Your Mail.</strong></p>';
                                    header('Location: ../user_login.php');
                                  } else {
                                    $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Something went wrong</strong></p>' ;
                                    header('Location: ../user_signup.php');
                                  }
            }
          }else{
            $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Robot verification failed, please try again.</strong></p>' ;
                                    header('Location: ../user_signup.php');
          }
          }else{
            $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Please check on the reCAPTCHA box.</strong></p>' ;
                                    header('Location: ../user_signup.php');
          }
    }
?>  