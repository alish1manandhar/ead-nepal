<?php include_once('header.php');
 $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
        $viewerDetail = mysqli_query($conn, $viewer);

        if (mysqli_num_rows($viewerDetail) > 0) {
            // output data of each row
            $row = mysqli_fetch_assoc($viewerDetail);
            
        }else{
          header('location: ../user_login.php');
        }
 ?>
  <!-- Start fact Area -->
  <section class="register section-gap-top">
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../user/audioHistory.php" style=" color: white;">Audio History</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">AUDIO HISTORY</h2>
        
      </div>
      <div class="row">
        <form action="" style="min-width: 100%;">
          <table id="example" class="table table-responsive" style="width:100%">
        <thead>
            <tr>
                <th>S.N</th>
                <th>Ad Name</th>
                <th>View Date</th>
            </tr>
        </thead>
        <tbody>
            
              <?php
              $historySql = "SELECT ads.id, ads.des, ads.audio, a_title as 'title', watch_date as 'date', ads.email, ads.phone, ads.url FROM add_audio_ads ads, audioviews im where $uid = im.fk_viewer and ads.id = im.fk_ads  ORDER BY im.id DESC";
                $historyResult = mysqli_query($conn, $historySql);
                while($data = mysqli_fetch_assoc($historyResult)):
                    $sn += 1;
               ?>
                 <tr>
                <td><?php echo $sn; ?></td>
                <td>
                    <a  href="#" class="add-to-cart" role="button" data-toggle="modal" data-target="#VideoAdsModel<?php echo $data['id']; ?>" ><?php echo $data['title']; ?></a>
                </td>
                <div class="modal fade" data-keyboard="false" data-backdrop="static"   id="VideoAdsModel<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="color: black;" align="center">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-body">
                      
                      <audio controls style="width: 100%;">
                          <source src="../../client/pages/target audio/uploads/<?php echo $data['audio']; ?>" type="audio/mp3">
                      </audio>

                    <div style="margin-top: 25px;">
                      <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12"  style="text-align: justify;">
                        <h5><?php echo $data['title']; ?></h5>
                        <p style="color: black;"><?php echo $data['des']; ?></p>
                      </div>
                      <hr>
                      <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                        <i class="fa fa-phone-square" style="color: black; font-size: 18px;"> <?php if ($data['phone'] == NULL) {
                          echo "No Contact";
                          }else{
                            echo $data['phone'];
                          } ?></i>
                      </div>

                      <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left" >
                        <i class="fa fa-envelope" style="color: black; font-size: 18px;"> <?php if ($data['email'] == NULL) {
                          echo "No Email";
                          }else{
                            echo "<a href='mailto:".$data['email']."' target='_blank'>Send Mail</a>";
                          } ?></i>

                      </div>

                      <div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" align="left">
                        <i class="fa  fa-globe" style="color: black; font-size: 18px;"> <?php if ($data['url'] == NULL) {
                          echo "No Website";
                          }else{
                            echo "<a href='https://".$data['url']."' target='_blank'>Visit Website</a>";
                          } ?></i>
                      </div>
                    </div>
                  </div>
                    <div class="modal-footer">
                      <button type="button" onClick="location.reload();" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
                <td><?php echo $data['date']; ?></td>
                </tr>
            <?php endwhile; ?>
            
        </tbody>
        <tfoot>
            <tr>
                <th>S.N</th>
                <th>Ad Name</th>
                <th>View Date</th>
            </tr>
        </tfoot>
    </table>
            <div style="margin-top: 5px;"> 
           <a href="./../EditUser/"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
        </form>
        
      </div>
    </div>
    
    
  </section>
<!-- Start Footer Area -->
  <footer class="footer-area section-gap-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
          <img src="./../images/icon_white.svg" style="width: 100px; height: 100px;"><br><br>
          <ul>
            <li>Got Questions?</li>
            <li>Call Us: 01-5548938</li>
            <li>Jhamshikhel, Lalitpur</li>
          </ul>
          <div class="footer_social">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
        </div>
        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12  single-footer-widget">
          <h4>Quick Links</h4>
          <ul>
            <li><a href="./../Picture/">Picture Ads</a></li>
            <li><a href="./../video/">Video Ads</a></li>
            <li><a href="./../audio/">Audio Ads</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-footer-widget">
          <div class="download-button flex-row ">
            <div class="buttons flex-row d-flex">
              <i class="fa fa-apple" aria-hidden="true"></i>
              <div class="desc">
                  <p>
                    <span>Soon</span> <br>
                    on App Store
                  </p>
              </div>
            </div><br>
            <div class="buttons dark flex-row d-flex">
              <i class="fa fa-android" aria-hidden="true"></i>
              <div class="desc">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.azwraithnp.eadnepal">
                  <p>
                    <span>Available</span> <br>
                    on Play Store
                  </p>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6  col-sm-6 col-xs-12  single-footer-widget">
          <h4>Newsletter</h4>
          <p>You can trust us. we only send promo offers,</p>
          <div class="form-wrap" id="mc_embed_signup">
            <form target="_blank" action=""
             method="get" class="form-inline">
              <input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
               required="" type="email">
              <button class="click-btn btn btn-default" style="margin-top: -20px;"><span class="lnr lnr-arrow-right"></span></button>
              
            </form>
          </div>
        </div>
      </div>
      <div class="footer-bottom row align-items-center">
        <p class="footer-text m-0 col-lg-12 col-md-12 col-sm-12 col-xs-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        
      </div>
    </div>
  </footer>
  <!-- End Footer Area -->


<script src="../js/modernizr-latest.js"></script> 
<script src="../js/jquery-1.8.2.min.js" type="text/javascript"></script> 
<script src="../js/bootstrap.min.js" type="text/javascript"></script> 
<script src="../js/jquery.isotope.min.js" type="text/javascript"></script> 
<script src="../js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script> 
<script src="../js/jquery.nav.js" type="text/javascript"></script> 
<script src="../js/jquery.fittext.js"></script> 
<script src="../js/waypoints.js"></script> 
<script src="../flexslider/jquery.flexslider.js"></script>
<script src="../js/custom.js" type="text/javascript"></script> 
<script src="../js/owl-carousel/owl.carousel.js"></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script  src="../js/form_anim.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script> 
     <script type="text/javascript">
      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
   </body>
   </html>