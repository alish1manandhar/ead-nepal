<?php
include_once'header.php';
session_start();
?>
	<!-- Start fact Area -->
	<section class="register section-gap-home">
		<div class="container">
			<div class="text-center" >
				<h2 style="color: white;">CLIENT SIGNUP</h2>
				<p style="color: white">(FIELDS WITH <b style="color: red;">*</b> SIGN ARE COMPULSORY)</p>
				<p style="color: white; margin-top: 10px;"><?php
				  if (isset($_SESSION['uploadErr'])) {
				    echo $_SESSION['uploadErr'];
				        unset($_SESSION['uploadErr']);
				  }
				  ?>
				</p>
			</div>
			<div class="row">
				<form action="./client/register.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
							<label>
							    <p class="label-txt">FIRST NAME <b style="color: red;">*</b></p><br>
							    <input type="text" id="fname" class="input" name="f_name" required/>
							    <div class="line-box">
							      <div class="line"></div>
							    </div>
							  </label>
						</div>
						<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
							<label>
						    <p class="label-txt">LAST NAME <b style="color: red;">*</b></p><br>
						    <input type="text" id="lname" class="input" name="l_name" required/>
						    <div class="line-box">
						      <div class="line"></div>
						    </div>
						  </label>
						</div>
					</div>

				  <label>
				    <p class="label-txt">EMAIL <b style="color: red;">*</b></p><br>
				    <input type="email" id="email" class="input" name="email" required/>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>
				  <label>
				    <p class="label-txt">CONTACT <b style="color: red;">*</b></p><br>
				    <input type="number" id="phone" class="input" name="p_number" required/>
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>
				  <div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
							<label>
						    <p class="label-txt">COMPANY NAME</p><br>
						    <input type="text" name="c_name" class="input" />
						    <div class="line-box">
						      <div class="line"></div>
						    </div>
						  </label>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
							<label>
						    <p class="label-txt">COMPANY TYPE</p><br>
						    <input type="text" name="c_type" class="input" />
						    <div class="line-box">
						      <div class="line"></div>
						    </div>
						  </label>
						</div>
						
					</div>

				  <label>
				    <p class="label-txt">PAN / CITIZENSHIP NUMBER <b style="color: red;">*</b></p><br>
				    <input type="text" id="c_pan" name="c_pan" class="input" required />
				    <div class="line-box">
				      <div class="line"></div>
				    </div>
				  </label>
				  <label>
				    <p class="label-txt">PAN / CITIZENSHIP IMAGE <b style="color: red;">*</b></p><br>
				    <input type="file" id="image" name="image" accept="image/*" required/>
				    <p style="font-size:13px; color: red;">(To submit copy of legal papers (MAX 20 MB)).</p>  
				    (By clicking Sign Up, you agree to our <a href="./terms&condition/">Terms And Conditions</a> and <a href="./terms&condition/privacy_policy.html">Privacy Policy</a>.)
				  </label>

				  <div class="row">
				  	<div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6Lco0agUAAAAAOq80GgmpD2on0xw7-wLGhP2NQO3"></div>
				  </div>

				  <div align="right"> 
				  	<button type="submit" name="submit" >SIGN UP</button>
				  </div>
				  
				</form>
				
			</div>
		</div>
		
		
	</section>
	<!-- End fact Area -->


<?php
	include_once'footer.php';
?>