<?php 
	include_once 'header.php';
   $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
      $viewerDetail = mysqli_query($conn, $userDetail);

      if (mysqli_num_rows($viewerDetail) > 0) {
          // output data of each row
          $row = mysqli_fetch_assoc($viewerDetail);
          
      }else{
        header('location: ../user_login.php');
      }

      if (isset($_POST['submit'])) {
      $radio = mysqli_real_escape_string($conn,ucfirst($_POST['radio']));
      $amount = mysqli_real_escape_string($conn,ucfirst($_POST['amount']));
      if($row['balance'] < $amount){
        $_SESSION['RedeemError'] = '<div class="alert alert-danger"><strong>You Do Not Have Stated Balance.</strong></div>';
      }
      elseif ($amount < 100) {
        $_SESSION['RedeemError'] = '<div class="alert alert-danger"><strong>Cant Redeem Less than NRs.100</strong></div>';
      }
      else{
        $newBalance = $row['balance'] - $amount;
        $updateBalance = "UPDATE viewers SET balance = '".$newBalance."' WHERE id = '".$uid."'";
        $prepareUpdate = mysqli_query($conn, $updateBalance);
        if ($prepareUpdate) {
        $redeemUser = "INSERT INTO redeem (userid, amount, redeemdate, redeemtype) VALUES ('$uid', '$amount', CURDATE(), '$radio')";
        $redeemDetail = mysqli_query($conn, $redeemUser);
        if ($redeemDetail) {
          $_SESSION['uploadErr'] = '<p class="alert alert-success"><strong>Success: Mail Sent To Admin. You Will Get Your Balance In Short Period.</strong></p>';
          
      }else{
        $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Balance Not Redeemed.</strong></p>';
      }
      }else{
      $_SESSION['uploadErr'] = '<p class="alert alert-danger"><strong>Error: Balance Not Redeemed.</strong></p>';
    }
      }
      }

      
   ?>

    <!-- Start fact Area -->
  <section class="register section-gap-top">
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../EditUser/redeembalance.php" style=" color: white;">Redeem Balance</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">BALANCE DETAILS</h2>
        
          <p style="color: white;">
            (*Note: Minimum Balance Should Be 100 Before You Can Redeem Your Balance.)
          </p>
      </div>
         
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['uploadErr'])) {
              echo $_SESSION['uploadErr'];
                  unset($_SESSION['uploadErr']);
            }

            if (isset($_SESSION['RedeemError'])) {
              echo $_SESSION['RedeemError'];
              unset($_SESSION['RedeemError']);
            }
          ?>
        </p>
      <div class="row">
        <form action="" method="post" enctype="multipart/form-data">

          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
              <label>
                <p class="label-txt">AMOUNT TO REDEEM</p><br>
                <input type="text" name="amount" class="input" required>
                <div class="line-box">
                  <div class="line"></div>
                </div>
              </label>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
              <label>
                <p class="label-txt">WAY TO REDEEM</p><br>
                <select name="radio" id="sex" class="input" style="min-height: 30px;" required>
                  <option value="Recharge Card">Recharge Card</option>
                  <option value="Gift Voucher">Gift Voucher</option>
                  <option value="Cash">Cash</option>
                </select>
              </label>
            </div>
          </div>

          <div align="right"> 
            <button type="submit" name="submit" >REDEEM</button>
          </div>
          
          <div style="margin-top: 5px;"> 
           <a href="./"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
          
        </form>
        
      </div>
    </div>
    
    
  </section>


<?php 
	include_once 'footer.php';
?>