<?php 
	include_once './header.php';
if (isset($_POST['submit'])) {
      $email2 = mysqli_real_escape_string($conn,ucfirst($_POST['email']));
      $checkEmail = sprintf("SELECT * FROM viewers WHERE email = '".$email2."'",
            mysqli_real_escape_string($conn,$email2)
      );
      $checkResult=$conn->query($checkEmail);
      $count = $checkResult->num_rows;
          if ($count ==1) {
              $_SESSION['EmailError'] = '<div class="alert alert-danger"><strong>Email Already Exist.</strong></div>';
          }else if($email2 == ""){
            $_SESSION['EmailError'] = '<div class="alert alert-danger"><strong>Please Insert Email.</strong></div>';
          }
          else{
            $update = "UPDATE viewers SET 
            email = '".$email2."'
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              unset($_SESSION['uemail']);
              $_SESSION['uemail'] = $email2;
              $_SESSION['EmailSuccess'] = '<div class="alert alert-success"><strong>Email Changed.</strong></div>';
              header("Location: ./");
            }else{
              $_SESSION['EmailError'] = '<div class="alert alert-danger"><strong>Email Update Failed.</strong></div>';
              header("Refresh:0");
            }
          }
}else{

}


?>
 <!-- Start fact Area -->
  <section class="register section-gap-top">
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../EditUser/editemail.php" style=" color: white;">Edit Email</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">EDIT EMAIL</h2>
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['EmailError'])) {
              echo $_SESSION['EmailError'];
              unset($_SESSION['EmailError']);
            }
          
          ?>
        </p>
      </div>
      <div class="row">
        <form action="" method="post" enctype="multipart/form-data">
          <label>
            <p class="label-txt">CURRENT EMAIL</p><br>
            <input name="" type="email" class="input" placeholder="<?php echo $email; ?>" readonly>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <label>
            <p class="label-txt">NEW EMAIL</p><br>
            <input  name="email" type="email" class="input" placeholder="someone@xyz.com" required>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>


          <div align="right"> 
            <button type="submit" name="submit" >CHANGE</button>
          </div>

          <div style="margin-top: 5px;"> 
           <a href="./"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
          
        </form>

        
      </div>
    </div>
    
    
  </section>
  <!-- End fact Area -->


<?php 
	include_once './footer.php';
?>