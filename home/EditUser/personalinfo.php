<?php 
  include_once 'header.php';
  $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
        $viewerDetail = mysqli_query($conn, $viewer);

        if (mysqli_num_rows($viewerDetail) > 0) {
            // output data of each row
            $row = mysqli_fetch_assoc($viewerDetail);
            
        }else{
          header('location: ../user_login.php');
        }
if (isset($_POST['submit'])) {
  $fname = mysqli_real_escape_string($conn,ucfirst($_POST['fname']));
  $lname = mysqli_real_escape_string($conn,ucfirst($_POST['lname']));
  $dob = mysqli_real_escape_string($conn,ucfirst($_POST['dob']));
  $sex = mysqli_real_escape_string($conn,ucfirst($_POST['sex']));
  $location = mysqli_real_escape_string($conn,ucfirst($_POST['location']));
  $college = mysqli_real_escape_string($conn,ucfirst($_POST['college']));
  $level = mysqli_real_escape_string($conn,ucfirst($_POST['level']));
  $field_of_study = mysqli_real_escape_string($conn,ucfirst($_POST['field_of_study']));

            $update = "UPDATE viewers SET 
            f_name = '".$fname."',
            l_name = '".$lname."',
            location = '".$location."',
            college = '".$college."',
            level = '".$level."',
            field_of_study = '".$field_of_study."',
            age = '".$age."',
            sex = '".$sex."'
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              $_SESSION['EditSuccess'] = '<div class="alert alert-success"><strong>Personal Information Updated.</strong></div>';
              header("Location: ./");
            }else{
              $_SESSION['EditError'] = '<div class="alert alert-danger"><strong>Personal Information Update Failed.</strong></div>';
              header("Refresh:0");
            }
}else{

}


?>
 <!-- Start fact Area -->
  <section class="register section-gap-top">
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../EditUser/personalinfo.php" style=" color: white;">Personal Info</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">PERSONAL INFORMATION</h2>
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['EditError'])) {
              echo $_SESSION['EditError'];
              unset($_SESSION['EditError']);
            }
          ?>
        </p>
      </div>
      <div class="row">
        <form action="" method="post" enctype="multipart/form-data">
          <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
          <label>
            <p class="label-txt">FIRST NAME</p><br>
            <input name="fname" type="text" class="input" value="<?php echo ucfirst($row['f_name']); ?>" required>
            <div class="line-box">
            <div class="line"></div>
            </div>
            </label>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
          <label>
            <p class="label-txt">LAST NAME</p><br>
            <input  name="lname" type="text" class="input" value="<?php echo ucfirst($row['l_name']); ?>" required>
            <div class="line-box">
             <div class="line"></div>
            </div>
            </label>
          </div>
         </div>

          <label>
            <p class="label-txt">DATE OF BIRTH</p><br>
            <input type="date" id="age" class="input" name="dob" value="<?php echo ucfirst($row['dob']); ?>" required/>
            <div class="line-box">
            <div class="line"></div>
            </div>
          </label>

          <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
              <label>
              <p class="label-txt">SEX <b style="color: red;">*</b></p><br>
              <select name="sex" id="sex" class="input" style="min-height: 30px;" required>
                <option value="<?php echo ucfirst($row['sex']); ?>"><?php echo ucfirst($row['sex']); ?> </option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
            </label>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
              <label>
              <p class="label-txt">LOCATION <b style="color: red;">*</b></p><br>
              <select name="location" id="location" class="input" style="min-height: 30px;">
                <option value="<?php echo ucfirst($row['location']); ?>"><?php echo ucfirst($row['location']); ?> </option>
                <option value="Achham">Achham</option>
                <option value="Arghakhanchi">Arghakhanchi</option>
                <option value="Baglung">Baglung</option>
                <option value="Baitadi">Baitadi</option>
                <option value="Bajhang">Bajhang</option>
                <option value="Bajura">Bajura</option>
                <option value="Banke">Banke</option>
                <option value="Bara">Bara</option>
                <option value="Bardiya">Bardiya</option>
                <option value="Bhaktapur">Bhaktapur</option>
                <option value="Bhojpur">Bhojpur</option>
                <option value="Chitwan">Chitwan</option>
                <option value="Dadeldhura">Dadeldhura</option>
                <option value="Dailekh">Dailekh</option>
                <option value="Dang">Dang deukhuri</option>
                <option value="Darchula">Darchula</option>
                <option value="Dhading">Dhading</option>
                <option value="Dhankuta">Dhankuta</option>
                <option value="Dhanusa">Dhanusa</option>
                <option value="Dholkha">Dholkha</option>
                <option value="Dolpa">Dolpa</option>
                <option value="Doti">Doti</option>
                <option value="Gorkha">Gorkha</option>
                <option value="Gulmi">Gulmi</option>
                <option value="Humla">Humla</option>
                <option value="Ilam">Ilam</option>
                <option value="Jajarkot">Jajarkot</option>
                <option value="Jhapa">Jhapa</option>
                <option value="Jumla">Jumla</option>
                <option value="Kailali">Kailali</option>
                <option value="Kalikot">Kalikot</option>
                <option value="Kanchanpur">Kanchanpur</option>
                <option value="Kapilvastu">Kapilvastu</option>
                <option value="Kaski">Kaski</option>
                <option value="Kathmandu">Kathmandu</option>
                <option value="Kavrepalanchok">Kavrepalanchok</option>
                <option value="Khotang">Khotang</option>
                <option value="Lalitpur">Lalitpur</option>
                <option value="Lamjung">Lamjung</option>
                <option value="Mahottari">Mahottari</option>
                <option value="Makwanpur">Makwanpur</option>
                <option value="Manang">Manang</option>
                <option value="Morang">Morang</option>
                <option value="Mugu">Mugu</option>
                <option value="Mustang">Mustang</option>
                <option value="Myagdi">Myagdi</option>
                <option value="Nawalparasi">Nawalparasi</option>
                <option value="Nuwakot">Nuwakot</option>
                <option value="Okhaldhunga">Okhaldhunga</option>
                <option value="Palpa">Palpa</option>
                <option value="Panchthar">Panchthar</option>
                <option value="Parbat">Parbat</option>
                <option value="Parsa">Parsa</option>
                <option value="Pyuthan">Pyuthan</option>
                <option value="Ramechhap">Ramechhap</option>
                <option value="Rasuwa">Rasuwa</option>
                <option value="Rautahat">Rautahat</option>
                <option value="Rolpa">Rolpa</option>
                <option value="Rukum">Rukum</option>
                <option value="Rupandehi">Rupandehi</option>
                <option value="Salyan">Salyan</option>
                <option value="Sankhuwasabha">Sankhuwasabha</option>
                <option value="Saptari">Saptari</option>
                <option value="Sarlahi">Sarlahi</option>
                <option value="Sindhuli">Sindhuli</option>
                <option value="Sindhupalchok">Sindhupalchok</option>
                <option value="Siraha">Siraha</option>
                <option value="Solukhumbu">Solukhumbu</option>
                <option value="Sunsari">Sunsari</option>
                <option value="Surkhet">Surkhet</option>
                <option value="Syangja">Syangja</option>
                <option value="Tanahu">Tanahu</option>
                <option value="Taplejung">Taplejung</option>
                <option value="Terhathum">Terhathum</option>
                <option value="Udayapur">Udayapur</option>
              </select> 
            </label>
            </div>
            
         </div>  

         <label>
            <p class="label-txt">COLLEGE</p><br>
            <input type="text" name="college" class="input" value="<?php echo ucfirst($row['college']); ?>">
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <div class="row">
            
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
            <label>
              <p class="label-txt">LEVEL <b style="color: red;">*</b></p><br>
              <select name="level" class="input" style="min-height: 30px;" >
                <option value="<?php echo ucfirst($row['level']); ?>"><?php echo ucfirst($row['level']); ?></option>
              <option value="slc/see">SLC/SEE</option>
              <option value="plus_two">+2 Higher Education</option>
              <option value="bachelors">Bachelors</option>
              <option value="masters">Master Degree</option>
            </select>
          </label>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
            <label>
              <p class="label-txt">FIELD OF STUDY <b style="color: red;">*</b></p><br>
              <select name="field_of_study" class="input" style="min-height: 30px;">
                <option value="<?php echo ucfirst($row['field_of_study']); ?>"><?php echo ucfirst($row['field_of_study']); ?> </option>
              <option value="commerce">Commerce</option>
              <option value="science">Science</option>
              <option value="it">IT</option>
              <option value="arts">Arts</option>
              <option value="others">Others</option>
            </select>
          </label>
            </div>
          </div>




          <div align="right"> 
            <button type="submit" name="submit" >CHANGE</button>
          </div>

          <div style="margin-top: 5px;"> 
           <a href="./"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
          
        </form>
        
      </div>
    </div>
    
    
  </section>
  <!-- End fact Area -->


<?php 
  include_once './footer.php';
?>