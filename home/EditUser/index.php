<?php 
	include_once './header.php';
   $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
  $viewerDetail = mysqli_query($conn, $viewer);

  if (mysqli_num_rows($viewerDetail) > 0) {
      // output data of each row
      $row = mysqli_fetch_assoc($viewerDetail);
      
  }else{
    header('location: ../user_login.php');
  }
 ?>
 <style type="text/css">
 .snip1519 {
  overflow: hidden;
  margin: 10px;
  max-width: 300px;
  min-height: 250px;
  max-height: 250px;
  width: 100%;
  background-color: #ffffff;
  border-radius: 5px;
  border-top: 5px solid #0e8ce4;
  color: #9e9e9e;
  text-align: center;
  font-size: 16px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
}

.snip1519 *,
.snip1519 *:before {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-transition: all 0.1s ease-out;
  transition: all 0.1s ease-out;
}

.snip1519 figcaption {
  padding: 12% 10% 14%;
}

.snip1519 img {
  font-size: 54px;
  color: #fff;
  width: 80px;
  line-height: 80px;
  border-radius: 20%;
  padding: 0;
  margin: 0 0 10px;
  text-align: center;
  display: inline-block;
}

.snip1519 h3 {
  color: #3c3c3c;
  font-size: 20px;
  font-weight: 300;
  line-height: 24px;
  margin: 10px 0;
}

.snip1519 p {
  font-size: 0.9em;
  font-weight: 300;
  margin: 0 0 20px;
}

.snip1519 .button {
  text-decoration: none;
  color: #777;
  border: 1px solid rgba(0, 0, 0, 0.3);
  border-radius: 5px;
  padding: 5px 10px;
  display: inline-block;
  position: relative;
  z-index: 1;
}

.snip1519 .button:before {
  background-color: #A9240F;
  position: absolute;
  top: 100%;
  bottom: 0;
  left: 0;
  right: 0;
  content: '';
  z-index: -1;
  opacity: 0;
}

.snip1519 .button:hover {
  color: #fff;
}

.snip1519 .button:hover:before {
  top: 0;
  opacity: 1;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important
}
     </style>

  <!-- Start fact Area -->
  <section class="register section-gap-top">

    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a>
      </div><br>
      <div class="text-center">
        <h3 style="color: white;">USER PROFILE</h3>

      </div>
      
      
      <div class="text-center" >
        
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['EmailSuccess'])) {
              echo $_SESSION['EmailSuccess'];
              unset($_SESSION['EmailSuccess']);
            }


            if (isset($_SESSION['ContactSuccess'])) {
              echo $_SESSION['ContactSuccess'];
              unset($_SESSION['ContactSuccess']);
            }

            if (isset($_SESSION['PasswordSuccess'])) {
              echo $_SESSION['PasswordSuccess'];
              unset($_SESSION['PasswordSuccess']);
            }

            if (isset($_SESSION['EditSuccess'])) {
              echo $_SESSION['EditSuccess'];
              unset($_SESSION['EditSuccess']);
            }
          
          ?>
        </p>
      </div>
      <div class="row" style="background-color: rgba(245, 245, 245, 0.7);">
        <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
          <figure class="snip1519">
          <figcaption><img src="../../assets/img/user_mail.png">
            <h3>Email</h3>
            <p><?php echo $email; ?></p><a href="./editemail.php">Change Email</a>
          </figcaption>
        </figure>
        </div>
         <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
           <figure class="snip1519">
          <figcaption><img src="../../assets/img/user_contact.png">
            <h3>Contact</h3>
            <p><?php echo $row['phone']; ?></p><a href="./editcontact.php">Change Contact</a>
          </figcaption>
        </figure>
         </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
          <figcaption><img src="../../assets/img/user_balance.png">
            <h3>Balance</h3>
             <p>Rs. <?php echo $row['balance']; ?></p>
             <?php if(!($row['balance'] < 100)): ?>
           <a href="./redeembalance.php">Redeem Balance</a>
         <?php endif; ?>
          </figcaption>
        </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/user_password.png">
                <h3>Password</h3>
               <a href="./editpassword.php">Change Password</a>
              </figcaption>
            </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/user_interest.png">
                <h3>Interest</h3>
               <a href="./interest.php">Change Interest</a>
              </figcaption>
            </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/user_info.png">
                <h3>General Info</h3>
               <a href="./personalinfo.php">Change Info</a>
              </figcaption>
            </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/picture_history.png">
                <h3>Picture History</h3>
               <a href="./../user/pictureHistory.php">View History</a>
              </figcaption>
            </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-6 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/video_history.png">
                <h3>Video History</h3>
               <a href="./../user/videoHistory.php">View History</a>
              </figcaption>
            </figure>
          </div>
          <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4">
            <figure class="snip1519">
              <figcaption><img src="../../assets/img/audio_history.png">
                <h3>Audio History</h3>
               <a href="./../user/audioHistory.php">View History</a>
              </figcaption>
            </figure>
          </div>
      </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px; margin-bottom: 20px" align="center">
            <a href="./../user/" style="color: white;"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
    </div>
    
    
  </section>

<?php 
	include_once './footer.php';
?>