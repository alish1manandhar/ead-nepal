<?php 
  include_once './header.php';
  $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
        $viewerDetail = mysqli_query($conn, $viewer);

        if (mysqli_num_rows($viewerDetail) > 0) {
            // output data of each row
            $row = mysqli_fetch_assoc($viewerDetail);
            
        }else{
          header('location: ../user_login.php');
        }

if (isset($_POST['submit'])) {
      $oldpass = md5(mysqli_real_escape_string($conn,$_POST['oldpass']));
      $newpass = mysqli_real_escape_string($conn,$_POST['newpass']);
      $confirm = mysqli_real_escape_string($conn,$_POST['confirm']);

        if (!($row['password'] == $oldpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Old Password Dont Match.</strong></div>';
        }else if(!($newpass == $confirm)){
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>New Password and Confirm Password Dont Match.</strong></div>';
        }elseif (strlen($newpass) < 9) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Length Must Be Greater Than 9</strong></div>';
        }elseif (!preg_match("#[0-9]+#",$newpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain A Number</strong></div>';
        }elseif(!preg_match("#[A-Z]+#",$newpass)) {
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Capital Alphabets</strong></div>';
        }elseif (!preg_match("#[a-z]+#",$newpass)){
          $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Must Contain Small Alphabets</strong></div>';
        }else{
          $newPassword = md5($newpass);
           $update = "UPDATE viewers SET 
           password = '".$newPassword."'
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              $_SESSION['PasswordSuccess'] = '<div class="alert alert-success"><strong>Password Changed.</strong></div>';
              header("Location: ./");
            }else{
              $_SESSION['PasswordError'] = '<div class="alert alert-danger"><strong>Password Update Failed.</strong></div>';
            }
        }
}else{

}


?>
 <!-- Start fact Area -->
  <section class="register section-gap-top">
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../EditUser/editpassword.php" style=" color: white;">Edit Password</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">EDIT PASSWORD</h2>
        <p style="color: white;">( Password Length Must Be Greater Than 9 And Must Contain A Number, Capital Letter And Small Letter)</p>
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['PasswordError'])) {
              echo $_SESSION['PasswordError'];
              unset($_SESSION['PasswordError']);
            }
          ?>
        </p>
      </div>
      <div class="row">
        <form action="" method="post" enctype="multipart/form-data">
          <label>
            <p class="label-txt">OLD PASSWORD</p><br>
            <input name="oldpass" type="password" class="input" required>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <label>
            <p class="label-txt">NEW PASSWORD</p><br>
            <input  name="newpass" type="password" class="input" required>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <label>
            <p class="label-txt">CONFIRM PASSWORD</p><br>
            <input  name="confirm" type="password" class="input" required>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <div align="right"> 
            <button type="submit" name="submit" >CHANGE</button>
          </div>

          <div style="margin-top: 5px;"> 
           <a href="./"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
          
        </form>
        
      </div>
    </div>
    
    
  </section>
  <!-- End fact Area -->


<?php 
  include_once './footer.php';
?>