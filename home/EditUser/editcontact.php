<?php 
	include_once './header.php';
if (isset($_POST['submit'])) {
      $contact2 = mysqli_real_escape_string($conn,ucfirst($_POST['contact']));
      $checkContact = sprintf("SELECT * FROM viewers WHERE phone = '".$contact2."'",
            mysqli_real_escape_string($conn,$contact2)
      );
      $checkResult=$conn->query($checkContact);
      $count = $checkResult->num_rows;
          if ($count ==1) {
              $_SESSION['ContactError'] = '<div class="alert alert-danger"><strong>Contact Already Exist.</strong></div>';
          }else if(strlen($contact2)<>10){
            $_SESSION['ContactError'] = '<div class="alert alert-danger"><strong>Contact Must Contain 10 Numbers.</strong></div>';
          }else{
            $update = "UPDATE viewers SET 
           phone = '".$contact2."'
            WHERE id = '".$uid."'";
            $prepareUpdate = mysqli_query($conn, $update);
            if ($prepareUpdate) {
              $_SESSION['ContactSuccess'] = '<div class="alert alert-success"><strong>Contact Changed.</strong></div>';
              header("Location: ./");
            }else{
              $_SESSION['ContactError'] = '<div class="alert alert-danger"><strong>Contact Update Failed.</strong></div>';
            }
          }
}else{

}

 $userDetail = "SELECT * FROM viewers where email = '$email' and id = $uid";
    $viewerDetail = mysqli_query($conn, $viewer);

    if (mysqli_num_rows($viewerDetail) > 0) {
        // output data of each row
        $row = mysqli_fetch_assoc($viewerDetail);
        
    }else{
      header('location: ../user_login.php');
    }
 ?>

  <!-- Start fact Area -->
  <section class="register section-gap-top">
    
    <div class="container">
      <div style="float: right; color: white;">
                <a href="./../user/" style=" color: white;">Home <i class="fa fa-home"></i></a> / <a href="./../EditUser/" style=" color: white;">Profile</a> / <a href="./../EditUser/editcontact.php" style=" color: white;">Edit Contact</a>
      </div><br>
      <div class="text-center" >
        <h2 style="color: white;">EDIT CONTACT</h2>
        <p style="color: white; margin-top: 10px;">
          <?php 
            if (isset($_SESSION['ContactError'])) {
              echo $_SESSION['ContactError'];
              unset($_SESSION['ContactError']);
            }
          
          ?>
        </p>
      </div>
      <div class="row">
        <form action="" method="post" enctype="multipart/form-data">
          <label>
            <p class="label-txt">CURRENT CONTACT</p><br>
            <input name="" type="text" class="input" placeholder="<?php echo $row['phone']; ?>" readonly>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>

          <label>
            <p class="label-txt">NEW CONTACT</p><br>
            <input  name="contact" type="text" class="input" required>
            <div class="line-box">
              <div class="line"></div>
            </div>
          </label>


          <div align="right"> 
            <button type="submit" name="submit" >CHANGE</button>
          </div>

          <div style="margin-top: 5px;"> 
           <a href="./"><i class="fa fa-arrow-left"> GO BACK </i></a>
          </div>
          
        </form>

        
      </div>
    </div>
    
    
  </section>

<?php 
	include_once './footer.php';
?>